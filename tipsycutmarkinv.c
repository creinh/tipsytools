/*
 * This program removes all marked particles from a binary file.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

#define LINE_SIZE 128

struct tipsyMarkData
{
    int nTot;
    int nGas;
    int nStar;
    int nMark;
    int *iOrder;
};

struct tipsyMarkData *tipsyReadMarkFile(char *Filename)
{
    struct tipsyMarkData *data;
    FILE *fp;
    int iRet;

    // Allocate memory and open the mark file
    data = calloc(1, sizeof(struct tipsyMarkData));
    
    if (data == NULL)
        return data;

    fp = fopen(Filename, "r");
    
    if (fp == NULL)
        return data;

    // Read the header
    iRet = fscanf(fp, "%d", &data->nTot);

    if (iRet <= 0)
        return data;

    iRet = fscanf(fp, "%d", &data->nGas);

    if (iRet <= 0)
        return data;

    iRet = fscanf(fp, "%d", &data->nStar);

    if (iRet <= 0)
        return data;

//  printf("%i %i %i\n", data->nTot, data->nGas, data->nStar);

    // Allocate memory for the marked particles
    data->iOrder = calloc(data->nTot, sizeof(int));

    if (data->iOrder == NULL)
        return data;

    data->nMark = 0;

//    fprintf(stderr, "Reading mark file: nMark= %i\n", data->nMark);

    /*
     * Read all marked particles (note that this code does not realize if the
     * format of the mark file is wrong!).
     */
    while (fscanf(fp, "%d", &data->iOrder[data->nMark]) == 1)
    {
//        printf("%i\n", data->iOrder[data->nMark]);
//        fprintf(stderr, "iOrder= %i, nMark= %i\n", data->iOrder[data->nMark], data->nMark);
        data->nMark++;
    }

    fclose(fp);

    return data;
}

int bIsParticleMarked(struct tipsyMarkData *data, int iOrder)
{
    int bMarked;
    int i;

    bMarked = FALSE;

    for (i=0; i<data->nMark; i++)
    {
        if (data->iOrder[i] == iOrder)
        {
            bMarked = TRUE;
            return bMarked;
        }
    }

    return bMarked;
}

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
        struct tipsyMarkData *data;
		int iRet, N_array;
		double rho;
        int i, j;

		// Check command line arguments
		if (argc != 4) {
			fprintf(stderr,"Usage: tipsycutmarkinv <input.std> <input.mark> <output.std>\n");
			exit(1);
		}

#if 0
        data = tipsyReadMarkFile("tipsy.mark");
        exit(1);
#endif

		// Initialize tipsy library
		TipsyInitialize(&in, 0, argv[1]);
		TipsyInitialize(&out, 0, NULL);

		// Read mark file
        data = tipsyReadMarkFile(argv[2]);
		assert(data != NULL);

        // Read the tipsy binary file header and check if the format agrees.
		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
        if (N != data->nTot)
        {
            fprintf(stderr, "Wrong file format.\n");
            exit(1);
        }

        j = 0;

        /*
         * Read all particles from the input file. Note that the marked
         * particle's ids start at 1 not at 0.
         */
        for (i=1; i<=N; i++)
        {
            p = pTipsyRead(in,&type,&dSoft);
            assert(type == TIPSY_TYPE_GAS);
/*
            if (type == TIPSY_TYPE_GAS)
			{
			} else {
				fprintf(stderr,"Particle %i is not a gas particle\n",i);
			}
*/
            if (!bIsParticleMarked(data, i))
            {
                g = (struct gas_particle *)p;

				// Add to output file
				TipsyAddGas(out,g);
                j++;
            }
		}

        fprintf(stderr, "Removed %i out of %i marked particles (N=%i).\n", j, data->nMark, N);

		// Write to stdout
        TipsyWriteAll(out, time, argv[3]);
		
		TipsyFinish(in);
		TipsyFinish(out);
}



