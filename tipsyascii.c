/*
 * Author:   Christian Reinhardt
 * Created:  09.12.2020
 *
 * This program reads a tipsy binary file and prints the particle data (e.g. pos, vel, mass,
 * pressure, temp) to an ASCII file.
 *
 * NOTE: Since the density is calculated during each SPH step a .den output file from Gasoline can
 * be provided as an optional command line argument to set the particle's density.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <EOSlib.h>
#include "tipsy.h"


void main(int argc, char **argv) {
    // Tipsy library
	TCTX in;
	int N;
	double time;
    // EOS library
    EOSMATERIAL **Mat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    int iMat;
    int nMat;
	double rho, u, P, T, cs;
    int bDenArray;
    int iRet;
    FILE *fp;
	int i, j;

    nMat = EOS_N_MATERIAL_MAX;

	if (argc < 3 || argc > 4) {
		fprintf(stderr,"Usage: tipsyascii <tipsy.std> <isT> <tipsy.den>\n");
		fprintf(stderr,"\n");
        fprintf(stderr,"isT specifies if the thermodynamical variable is u or T");
		fprintf(stderr,"Note: the argument is optional. If tipsy.den is\n");
		fprintf(stderr,"not specified the particles density are directly\n");
		fprintf(stderr,"read from the binary file.\n");
		exit(1);
	}
	
    // Print version information
    fprintf(stderr,"EOS library version: %s\n", EOS_VERSION_TEXT);

	if (argc == 4) {
		// The user specified tipsy.den.
        bDenArray = TRUE;
	} else {
        bDenArray = FALSE;
	}
	
    // Allocate memory for the array that containts all materials in the EOS library
    Mat = (EOSMATERIAL**) calloc(nMat, sizeof(EOSMATERIAL *));
    fprintf(stderr, "Memory for the EOS library is allocated (nMat= %i).\n", nMat);

	// Initialize the tipsy library.
	TipsyInitialize(&in, 0, argv[1]);

	N = iTipsyNumParticles(in);
	time = dTipsyTime(in);
    
    int isT = atoi(argv[2]);
    // Read all particles
    TipsyReadAll(in);

    // Open the density array file if needed.
    if (bDenArray) {
        fp = fopen(argv[3], "r");
        assert(fp != NULL);

        /*
         * Read the array file header and assert that the number of particles
         * is consistent.
         */
        iRet = fscanf(fp, "%d", &i);

        assert(i == N);
    }
 
	// Print header
    printf("#");
    printf(" x y z");
    printf(" vx vy vz");
    printf(" m");
    printf(" rho");
    printf(" u");
    printf(" P");
    printf(" T");
    printf(" cs");
    printf(" iMat");
    printf("\n");

    for (i = 0; i < N; i++) {
        // Read the density either from tipsy.den or directly from the binary file.
        if (bDenArray) {
            iRet = fscanf(fp, "%lf", &rho);
            assert(iRet > 0);
            assert(rho >= 0.0);
        } else {
            rho = in->gp[i].rho;
        }

		u = in->gp[i].temp;
		iMat = (int) in->gp[i].metals;

        // Initialize the material and look up table if needed.
        if (Mat[iMat] == NULL)
        {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);
            if (iMat == MAT_IDEALGAS) {
                struct igeosParam param;
                param.dConstGamma = 5.0/3.0;
                param.dMeanMolMass = 1.0;
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, &param);
            } else {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
            }
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        if (Mat[iMat]->bEntropyTableInit == EOS_FALSE)
        {
            if (iMat != MAT_IDEALGAS)
            {
                fprintf(stderr, "iMat %i entropy lookup table not yet initialized.\n", iMat);
                EOSinitIsentropicLookup(Mat[iMat], NULL);
                fprintf(stderr, "\n");
            }
        }

        if (isT) {
            u = EOSUofRhoT(Mat[iMat], rho, u);
        }
        P = EOSPofRhoU(Mat[iMat], rho, u);
        T = EOSTofRhoU(Mat[iMat], rho, u);
        cs = EOSCofRhoU(Mat[iMat], rho, u);
        
        for (j=0; j<3; j++) {
            printf("%15.7E", in->gp[i].pos[j]);
        }

        for (j=0; j<3; j++) {
            printf("%15.7E", in->gp[i].vel[j]);
        }

        printf("%15.7E", in->gp[i].mass);
        printf("%15.7E", rho);
        printf("%15.7E", u);
        printf("%15.7E", P);
        printf("%15.7E", T);
        printf("%15.7E", cs);
        printf("%5i", iMat);
        printf("\n");
	}
		
	// Free memory
	for (i=0; i<nMat; i++)
	{
        if (Mat[i] != NULL) EOSfinalizeMaterial(Mat[i]);
	}
    
    TipsyFinish(in);
}
