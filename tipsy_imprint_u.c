/*
** This program reads a Tipsy binary file and sets the particles internal energy
** to match a given profile.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"
#include "model.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		// Model
		MODEL *model;
		double time, R, u;
		int i, j;

		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_imprint_profile <tipsy.model> <tipsy.std\n");
			exit(1);
		}

		// Read model
		model = modelInit();
		modelRead(model,argv[1]);

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
		// Read particles from the input file
        for (i = 0; i < N; i++)
        {
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
			
			R = 0.0;

			for (j=0; j<3; j++)
			{
				R += g->pos[j]*g->pos[j];
			}	
			
			R = sqrt(R);

			// Expected rho and u from the model
			u = uLookup(model,R);
			
			g->temp = u;
			TipsyAddGas(out,g);
		}

		// Write to stdout
 		TipsyWriteAll(out,time,NULL);

		fprintf(stderr,"t=%g\n",time);
		TipsyFinish(in);
		TipsyFinish(out);
}
