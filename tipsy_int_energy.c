/*
 * This program calculates the total specific internal energy of a Tipsy binary file.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <EOSlib.h>
#include "tipsy.h"

#define INDEX(i, j) (((i)*granite->nTableV) + (j))

double dGmPerCcUnit(double dKpcUnit, double dMsolUnit)
{
    const double KBOLTZ = 1.38e-16;      /* bolzman constant in cgs */
    const double MHYDR = 1.67e-24;       /* mass of hydrogen atom in grams */
    const double MSOLG = 1.99e33;        /* solar mass in grams */
    const double GCGS = 6.67e-8;         /* G in cgs */
    const double KPCCM = 3.085678e21;    /* kiloparsec in centimeters */
    const double NA = 6.022e23;          /* Avogadro's number */
    double dGasConst;
    double dErgPerGmUnit;
    double dGmPerCcUnit;
    double dSecUnit;

    /*
     ** Convert kboltz/mhydrogen to system units, assuming that
     ** G == 1.
     */
    dGasConst = dKpcUnit*KPCCM*KBOLTZ/MHYDR/GCGS/dMsolUnit/MSOLG;
    /* code energy per unit mass --> erg per g */
    dErgPerGmUnit = GCGS*dMsolUnit*MSOLG/(dKpcUnit*KPCCM);
    /* code density --> g per cc */
    dGmPerCcUnit = (dMsolUnit*MSOLG)/pow(dKpcUnit*KPCCM,3.0);
    /* code time --> seconds */
    dSecUnit = sqrt(1/(dGmPerCcUnit*GCGS));

    return dGmPerCcUnit;
}




int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int type, N;
    double dSoft;
    struct base_particle *p;
    struct gas_particle *g;
    double time;
    // EOS library
    EOSMATERIAL **Mat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    // These values are currently hard coded in EOSlib
    //double rhomax = 200.0;
    //double vmax = 1200.0;
    //int nTableRho = 1000;
    //int nTableV = 1000;
    double rho_min = 0.0;
    int iMat;
    int nMat;
    double M = 0.0;
    double M_cond = 0.0;
    double M_exp = 0.0;
    // Total internal energy
    double u_tot = 0.0;
    // Total thermal energy
    double u_therm = 0.0;
    // Internal energy above and below rho_min
    double u_cond = 0.0;
    double u_exp = 0.0;
    int bLookupFail;
    int iRet;
    int i;

    nMat = EOS_N_MATERIAL_MAX;
    bLookupFail = 0;

    if (argc != 2) {
        fprintf(stderr,"Usage: tipsy_int_energy <rho_min> <tipsy.std\n");
        exit(1);
    }

    rho_min = atof(argv[1]);
    assert(rho_min >= 0.0);

    fprintf(stderr, "rho_min= %15.7E g/cm^3", rho_min);
    rho_min /= dGmPerCcUnit(dKpcUnit, dMsolUnit);
    fprintf(stderr, "= %15.7E (code units)\n", rho_min);

    // Print version information
    fprintf(stderr,"EOS library version: %s\n", EOS_VERSION_TEXT);

    // Allocate memory for the array that containts all materials in the EOS library
    Mat = (EOSMATERIAL**) calloc(nMat, sizeof(EOSMATERIAL *));
    fprintf(stderr, "Memory for the Tillotson library is allocated (nMat= %i).\n", nMat);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Read particles from the input file
    for (i=0; i<N; i++)
    {
        p = pTipsyRead(in, &type, &dSoft);
        assert(type == TIPSY_TYPE_GAS);
        g = (struct gas_particle *)p;

        // Check that a particles density is larger than TILL_RHO_MIN.
        if (g->rho < TILL_RHO_MIN)
            fprintf(stderr, "Particle %i: rho= %g is smaller than TILL_RHO_MIN.\n", i, g->rho);

        iMat = (int) g->metals;
        assert(iMat <= nMat);

        // CR
        fprintf(stderr, "i=%i iMat= %i\n", i, iMat);


        // Initialize the material and look up table if needed.
        if (Mat[iMat] == NULL)
        {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);
            if (iMat == MAT_IDEALGAS) {
                struct igeosParam param;
                param.dConstGamma = 5.0/3.0;
                param.dMeanMolMass = 1.0;
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, &param);
            } else {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
            }
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "\n");
        }

        if (Mat[iMat]->bEntropyTableInit == EOS_FALSE)
        {
            if (iMat != MAT_IDEALGAS)
            {
                fprintf(stderr, "iMat %i entropy lookup table not yet initialized.\n", iMat);
                EOSinitIsentropicLookup(Mat[iMat], NULL);
                fprintf(stderr, "\n");
            }
        }
        // Note that for our version of Gasoline the internal energy is stored in temp.
        u_tot += g->temp;
        M += g->mass;

        if (g->rho >= rho_min)
        {
            u_cond += g->temp;
            M_cond += g->mass;
        } else {
            u_exp += g->temp;
            M_exp += g->mass;
        }

        // Calculate the thermal part of the internal energy.
        if (iMat != MAT_IDEALGAS)
        {
            iRet = EOSIsInTable(Mat[iMat], g->rho, g->temp);

            if (iRet == EOS_SUCCESS)
            {
                u_therm += EOSUCold(Mat[iMat], g->rho); 
            } else {
                fprintf(stderr, "Failed to calculate thermal energy (iOrder= %i, rho= %g, u= %g, iMat= %i, iRet= %i).\n",
                        i, g->rho, g->temp, iMat, iRet);
                bLookupFail = 1;
            }
        } else {
            // Currently not implemented in EOSlib
            assert(0);
            // The ideal gas EOS does not model particle interaction.
            u_therm += g->temp;
        }

    }

    // Print the header
    printf("#             t");
    printf("        rho_min");
    printf("           utot");
    printf("       uthermal");
    printf("         u_cond");
    printf("          u_exp");
    printf("              M");
    printf("         M_cond");
    printf("          M_exp");
    printf("           N");
    printf("\n");

    printf("%15.7E", time);
    printf("%15.7E", rho_min);
    printf("%15.7E", u_tot);
    printf("%15.7E", u_therm);
    printf("%15.7E", u_cond);
    printf("%15.7E", u_exp);
    printf("%15.7E", M);
    printf("%15.7E", M_cond);
    printf("%15.7E", M_exp);
    printf("%12i", N);
    printf("\n");

    for (i=0; i<nMat; i++)
    {
        if (Mat[i] != NULL)
            EOSfinalizeMaterial(Mat[i]);
    }

    TipsyFinish(in);

    return 0;
}
