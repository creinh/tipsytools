/*
 * This program calculates the center of mass of a tipsy binary file.
 *
 * Author:  Christian Reinhardt
 * Created: 14.01.2019
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "tipsy.h"

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int type, N;
    double dSoft;
    struct base_particle *p;
    double time;
    double rcom[3];
    double vcom[3];
    double m;
    int i, j;

    // Check command line arguments
    if (argc != 1) {
        fprintf(stderr, "Usage: tipsy_com < input.std\n");
        exit(1);
    }

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);


    for (i=0; i<3; i++)
	{
		rcom[i] = 0.0;
		vcom[i] = 0.0;
	}

    m = 0.0;

    // Read all particles from the input file
    for (i=0; i<N; i++)
    {
        p = pTipsyRead(in, &type, &dSoft);
        for (j=0; j<3; j++)
        {
            rcom[j] += p->mass*p->pos[j];
            vcom[j] += p->mass*p->vel[j];
        }

        m += p->mass;
    }

    for (i=0; i<3; i++)
	{
		rcom[i] /= m;
		vcom[i] /= m;
	}

    printf("%.7e %.7e %.7e %.7e %.7e %.7e %.7e %.7e %.7e\n",time,rcom[0],rcom[1],rcom[2],sqrt(rcom[0]*rcom[0] + rcom[1]*rcom[1] + rcom[2]*rcom[2]),vcom[0],vcom[1],vcom[2],sqrt(vcom[0]*vcom[0] + vcom[1]*vcom[1] + vcom[2]*vcom[2]));

    TipsyFinish(in);

    return 0;
}



