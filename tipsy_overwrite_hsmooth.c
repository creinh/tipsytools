/*
** This program reads a Tipsy binary file and sets hsmooth read from a file
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

int main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		double hsmooth;
		int i, iRet;
		FILE *fp;

		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_overwrite_hsmooth <tipsy.hsm> <tipsy.std\n");
			exit(1);
		}


		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		fp = fopen(argv[1], "r");
		assert(fp != NULL);
		iRet = fscanf(fp, "%d", &i);
		// Read particles from the input file
	        for (i = 0; i < N; i++)
        	{
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
			
			iRet = fscanf(fp, "%lf", &hsmooth);
			assert(iRet > 0);
			
			g->hsmooth = hsmooth;
			TipsyAddGas(out,g);
		}

		// Write to stdout
 		TipsyWriteAll(out,time,NULL);

		fprintf(stderr,"t=%g\n",time);
		TipsyFinish(in);
		TipsyFinish(out);
		return 0;
}
