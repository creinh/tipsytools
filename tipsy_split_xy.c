/*
 * This program reads a Tipsy input file and converts iMat, so that the
 * resulting binary file is compatible with the new material numbers in the
 * Tillotson EOS library.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void main(int argc, char **argv)
{
    // Tipsy library
	TCTX in, outNW, outNE, outSW, outSE;
	int type;
	unsigned N;
	double dSoft, time;
	struct base_particle *p;
	struct gas_particle *g;
        unsigned i;

	// Check command line arguments
	if (argc != 6)
    {
		fprintf(stderr,"Usage: tipsy_split_xy <input.std> <outputNW.std> <outputNE.std> <outputSW.std> <outputSE.std>\n");
		exit(1);
	}

	// Initialize tipsy library
	TipsyInitialize(&in, 0, argv[1]);
	TipsyInitialize(&outNW, 0, NULL);
	TipsyInitialize(&outNE, 0, NULL);
	TipsyInitialize(&outSW, 0, NULL);
	TipsyInitialize(&outSE, 0, NULL);

        N = iTipsyNumParticles(in);
	time = dTipsyTime(in);

	
	/*
     * Read all particles from the input file, change the material id and write
     * them to the output.
     */
    for (i = 0; i < N; i++)
    {
        p = pTipsyRead(in,&type,&dSoft);
        assert(type == TIPSY_TYPE_GAS);
	if ( i % 1000000u == 0u) {
		fprintf(stderr,"%u M\n",i / 1000000u);
	}
        g = (struct gas_particle *)p;
	if (g->pos[0] >= -1.0f) {
		if (g->pos[1] >= -1.0f) {
			TipsyAddGas(outNE, g);
		}
		if (g->pos[1] <= 1.0f) {
			TipsyAddGas(outSE, g);
		}
	}

	if (g->pos[0] <= 1.0f) {
		if (g->pos[1] >= -1.0f) {
                        TipsyAddGas(outNW, g);
                }
                if (g->pos[1] <= 1.0f) {
                        TipsyAddGas(outSW, g);
                }
	}

    }

    TipsyFinish(in);

    fprintf(stderr,"Writing %u particles to %s\n",iTipsyNumParticles(outNW),argv[2]);
    TipsyWriteAll(outNW, time, argv[2]);
    TipsyFinish(outNW);
    fprintf(stderr,"Writing %u particles to %s\n",iTipsyNumParticles(outNE),argv[3]);
    TipsyWriteAll(outNE, time, argv[3]);
    TipsyFinish(outNE);
    fprintf(stderr,"Writing %u particles to %s\n",iTipsyNumParticles(outSW),argv[4]);
    TipsyWriteAll(outSW, time, argv[4]);
    TipsyFinish(outSW);
    fprintf(stderr,"Writing %u particles to %s\n",iTipsyNumParticles(outSE),argv[5]);
    TipsyWriteAll(outSE, time, argv[5]);
    TipsyFinish(outSE);
}



