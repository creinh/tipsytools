/*
 * Author:   Christian Reinhardt
 * Created:  07.05.2020
 * Modified: 28.12.2022 
 *
 * This program corrects an SPH representation of an equilibrium model, so that all particles are
 * on the same isentrope eventhough their densities (calculated from SPH) are not consistent with 
 * the 1D model due to noise.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <argp.h>
#include <EOSlib.h>
#include "tipsy.h"

struct BallicModel {
    double *R;
    double *rho;
    double *u;
    double *M;
    int *iMat;
    double *P;
    double *T;
    int nTable;
};

struct TipsyArray {
    int N;
    double *rho;
};


/*
 * Parse command line arguments with GNU argp.
 */
const char *argp_program_version = "tipsy_fix_model v1.0";
const char *argp_program_bug_address = "christian.reinhardt@ics.uzh.ch";

/* Description of the program. */
static char doc[] = "Fix the internal energy or temperature of a ballic model so that all particles are on an isentrope.";

/* Description of input arguments. */
static char args_doc[] = "[Binary] [Density] [Model]";

/* Define number of required arguments. */
#define NUM_ARGS_MAX 100

/* Start from 256 because we do not want short options. */
enum OPTIONS {
    OPT_SET_KPCUNIT = 256,
    OPT_SET_MSOLUNIT,
    OPT_SET_RHOSURF,
    OPT_SET_GAMMA,
    OPT_SET_MEANMOLMASS,
    OPT_SET_WRITETEMP
};

/* Optional arguments. */
static struct argp_option options[] = {
    { "dKpcUnit", OPT_SET_KPCUNIT, "KPCUNIT", 0, "Set length unit in kpc (default: 2.06701e-13)." },
    { "dMsolUnit", OPT_SET_MSOLUNIT, "MSOLUNIT", 0, "Set mass unit in Solar masses (default: 4.80438e-08)." },
    { "dConstGamma", OPT_SET_GAMMA, "GAMMA", 0, "Set gamma for the ideal gas eos (default: 5/3)." },
    { "dMeanMolMass", OPT_SET_MEANMOLMASS, "MEANMOLMASS", 0, "Set mean molecular mass in units of m_H (default: 1.0)." },
    { "bWriteTemp", OPT_SET_WRITETEMP, 0, 0, "Use temperature as output (as required by pkdgrav3)." },
    { 0 }
};

struct arguments {
    /* Required arguments. */
    char *args[NUM_ARGS_MAX];
    int nArgs;
    /* Options. */
    double dKpcUnit;
    double dMsolUnit;
    bool bKpcUnitSet;
    bool bMsolUnitSet;
    double dConstGamma;
    double dMeanMolMass;
    bool bWriteTemp;
};

error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
        case OPT_SET_KPCUNIT:
            arguments->bKpcUnitSet = true;
            arguments->dKpcUnit = atof(arg);
            break;
        case OPT_SET_MSOLUNIT:
            arguments->bMsolUnitSet = true;
            arguments->dMsolUnit = atof(arg);
            break;
        case OPT_SET_GAMMA:
            arguments->dConstGamma = atof(arg);
            break;
        case OPT_SET_MEANMOLMASS:
            arguments->dMeanMolMass = atof(arg);
            break;
        case OPT_SET_WRITETEMP:
            arguments->bWriteTemp = true;
            break;
        case ARGP_KEY_ARG:
            /* Too many arguments. */
            if (state->arg_num >= arguments->nArgs) {
                argp_usage(state);
            }
            arguments->args[state->arg_num] = arg;    
            break;
        case ARGP_KEY_END:
            if (state->arg_num < arguments->nArgs) {
                /* Not enough arguments. */
                argp_usage(state);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

struct BallicModel *ReadBallicModel(char *chFile) {
    struct BallicModel *model;
    FILE *fp;
    char *chLine;
    size_t nCharMax = 256;
    int iRet;
    int nTableMax = 100000;
    int i;

    /* Allocate memory. */
    model = (struct BallicModel *) calloc(1, sizeof(struct BallicModel));
    assert(model != NULL);

    model->R = (double *) calloc(nTableMax, sizeof(double));
    assert(model->R != NULL);
    model->rho = (double *) calloc(nTableMax, sizeof(double));
    assert(model->rho != NULL);
    model->u = (double *) calloc(nTableMax, sizeof(double));
    assert(model->u != NULL);
    model->M = (double *) calloc(nTableMax, sizeof(double));
    assert(model->M != NULL);
    model->iMat = (int *) calloc(nTableMax, sizeof(int));
    assert(model->iMat != NULL);
    model->P = (double *) calloc(nTableMax, sizeof(double));
    assert(model->P != NULL);
    model->T = (double *) calloc(nTableMax, sizeof(double));
    assert(model->T != NULL);
    model->nTable = 0;

    chLine = (char *) calloc(nCharMax, sizeof(char));

    /* Open the file. */
    fp = fopen(chFile, "r");
    assert(fp != NULL);

    i = 0;
    while (getline(&chLine, &nCharMax, fp) != -1) {
        /* Check if its a comment. */
        if (strchr(chLine, '#') != NULL) continue;

        iRet = sscanf(chLine, "%lf %lf %lf %lf %d %lf %lf", &model->R[i], &model->rho[i],
                      &model->M[i], &model->u[i],  &model->iMat[i], &model->P[i], &model->T[i]);

        /* Check if the number of matches is correct. */
        assert(iRet == 7);
        
        /* Check that the values are sensible. */
        assert(model->R[i] >= 0.0);
        assert(model->rho[i] >= 0.0);

        i++;

        assert(i < nTableMax);
    }

    model->nTable = i;

    fprintf(stderr, "ReadBallicModel: read %i lines.\n", model->nTable);

    return model;
}

struct TipsyArray *ReadTipsyArray(char *chFile) {
    struct TipsyArray *array;
    FILE *fp;
    char *chLine;
    size_t nCharMax = 256;
    int iRet;
    int i;

    /* Allocate memory. */
    array = (struct TipsyArray *) calloc(1, sizeof(struct TipsyArray));
    assert(array != NULL);
   
    array->N = 0;

    chLine = (char *) calloc(nCharMax, sizeof(char));

    /* Open the file. */
    fp = fopen(chFile, "r");
    assert(fp != NULL);

    /* Read header. */
    assert(getline(&chLine, &nCharMax, fp) != -1);
    iRet = sscanf(chLine, "%d", &array->N);
    assert(iRet == 1);

    assert(array->N > 0);

    array->rho = (double *) calloc(array->N, sizeof(double));
    assert(array->rho != NULL);

    for (i=0; i<array->N; i++) {
        assert(getline(&chLine, &nCharMax, fp) != -1);

        iRet = sscanf(chLine, "%lf", &array->rho[i]);
        assert(iRet == 1);
    }

    free(chLine);

    return array;
}

int FindRhoIndex(double rho, double* rhoAxis, int nRho)
{
    int i;
    if (rho >= rhoAxis[0]) return 0;
    if (rho <= rhoAxis[nRho-1]) return nRho-1;
    int a = 0;
    int b = nRho-2;
    int c = (b + a) / 2;
    while ((b - a) > 2) {
        if (rho < rhoAxis[c]) a = c;
        else b = c;
        c = (b + a) / 2;
    }
    int istart = c-5;
    if (istart < 0) istart = 0;
    for (i=istart; i<nRho-1; i++) {
        if ((rho <= rhoAxis[i]) && (rho >= rhoAxis[i+1])) break;
    }

    /* Check if rho_i or rho_i+1 are closer to rho. */
    if (fabs(rho-rhoAxis[i]) > fabs(rho-rhoAxis[i+1])) return i+1;

    return i;
}


int main(int argc, char **argv) {
    // Tipsy library
	TCTX in, out;
	int N;
	double time;
    // EOS library
    EOSMATERIAL **Mat;
    double dKpcUnit;
    double dMsolUnit;
    int iMat;
    int nMat;
    // Tipsy array file
    struct TipsyArray *array;
    // ballic model
    struct BallicModel *model;
    int iRho;
    double rho1;
    double rho2;
    double u1;
    double u2;
    struct arguments arguments;
    bool bWriteTemp;
    struct igeosParam igParam;
	int i;

    nMat = EOS_N_MATERIAL_MAX;

    /* Set number of required arguments. */
    arguments.nArgs = 3;

    /* Initialize default arguments. */
    arguments.dKpcUnit = 2.06701e-13;
    arguments.dMsolUnit = 4.80438e-08;
    arguments.bKpcUnitSet = false;
    arguments.bMsolUnitSet = false;
    arguments.dConstGamma = 5.0/3.0;
    arguments.dMeanMolMass = 1.0;
    arguments.bWriteTemp = false;
    
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (arguments.bKpcUnitSet != arguments.bMsolUnitSet) {
        fprintf(stderr, "Error: only one unit (dKpcUnit or dMsolUnit) was specified by the user.\n");
        exit(1);
    }

    dKpcUnit = arguments.dKpcUnit;
    dMsolUnit = arguments.dMsolUnit;
    
    /* Parameters for ideal gas. */    
    igParam.dConstGamma = arguments.dConstGamma;
    igParam.dMeanMolMass = arguments.dMeanMolMass;

    bWriteTemp = arguments.bWriteTemp;

    /* Print version information. */
    fprintf(stderr,"EOS library version: %s\n", EOS_VERSION_TEXT);

    /* Allocate memory for the array that containts all materials in the EOS library. */
    Mat = (EOSMATERIAL**) calloc(nMat, sizeof(EOSMATERIAL *));
    fprintf(stderr, "Memory for the EOS library is allocated (nMat= %i).\n", nMat);

	/* Initialize the tipsy library. */
	TipsyInitialize(&in, 0, arguments.args[0]);
	TipsyInitialize(&out, 0, NULL);

	N = iTipsyNumParticles(in);
	time = dTipsyTime(in);
    
    TipsyReadAll(in);

    /* Read density from tipsy array file. */
    array = ReadTipsyArray(arguments.args[1]);  
    assert(N == array->N);

    /* Read equilibrium model. */
    model = ReadBallicModel(arguments.args[2]);

    for (i = 0; i<N; i++) {	
		iMat = (int) in->gp[i].metals;
        //rho2 = in->gp[i].rho;
        rho2 = array->rho[i];

        iRho = FindRhoIndex(rho2, model->rho, model->nTable);
        rho1 = model->rho[iRho];
        u1 = model->u[iRho];
        
        // Initialize the material and look up table if needed.
        if (Mat[iMat] == NULL) {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);

            if (iMat == EOSIDEALGAS) {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, &igParam);
            } else {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
            }
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        if (Mat[iMat]->bEntropyTableInit == EOS_FALSE) {
            if (iMat != MAT_IDEALGAS)
            {
                fprintf(stderr, "iMat %i entropy lookup table not yet initialized.\n", iMat);
                EOSinitIsentropicLookup(Mat[iMat], NULL);
                fprintf(stderr, "\n");
            }
        }

        u2 = EOSIsentropic(Mat[iMat], rho1, u1, rho2);

        if (bWriteTemp) {
            in->gp[i].temp = EOSTofRhoU(Mat[iMat], rho2, u2);
        } else {
            in->gp[i].temp = u2;
        }

        TipsyAddGas(out, &in->gp[i]);
    }
	
    // Write all particles
	TipsyWriteAll(out, time, NULL);
	
    TipsyFinish(in);
    TipsyFinish(out);

	// Free memory
	for (i=0; i<nMat; i++)
	{
        if (Mat[i] != NULL) EOSfinalizeMaterial(Mat[i]);
	}

    free(array);
    free(model);

    return 0;
}
