/*
 * This program moves the com to a given point.
 *
 * Author: Christian Reinhardt
 * Date:   09.07.2021
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in,out;
    int type, N;
    double dSoft;
    struct base_particle *p;
    struct gas_particle *g;
    double time;
    double dx,dy,dz;
    int i;

    // Check command line arguments
    if (argc != 4) {
        fprintf(stderr, "Usage: tipsy_move_com <dx> <dy> <dz> < input.std\n");
        fprintf(stderr, "Note: All distances are in code units!\n");
        exit(1);
    }

    dx = atof(argv[1]);
    dy = atof(argv[2]);
    dz = atof(argv[3]);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");
    TipsyInitialize(&out, 0, NULL);

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Read all particles from the input file
    for (i = 0; i < N; i++)
    {
        p = pTipsyRead(in,&type,&dSoft);
        assert(type == TIPSY_TYPE_GAS);

        if (type == TIPSY_TYPE_GAS)
        {
            // Move the gas particle
            g = (struct gas_particle *)p;
            g->pos[0] += dx;
            g->pos[1] += dy;
            g->pos[2] += dz;
            TipsyAddGas(out,g);
        } else {
            fprintf(stderr,"Particle %i is not a gas particle\n",i);
        }
    }

    // Write to stdout
    TipsyWriteAll(out,time,NULL);

    TipsyFinish(in);
    TipsyFinish(out);

    return 0;
}



