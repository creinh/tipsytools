/*
** This program reads a Tipsy array file and outputs all particles with a given value to a Tipsy binary file.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, Ntot;
		double dSoft;
		double time, dMin, dMax, dVal;
		FILE *fp;
		int iRet,i;

		if (argc != 4) {
			fprintf(stderr,"Usage: tipsyarray <array.txt> <dMin> <dMax> <tipsy.std\n");
			exit(1);
		}
		
		dMin = atof(argv[2]);		
		dMax = atof(argv[3]);		
	
		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);
	
		// Read Tipsy binary header	
		//Ntot = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
		// Read all particles from the input file
		TipsyReadAll(in);

		// Open the file.
		fp = fopen(argv[1],"r");
		assert(fp != NULL);

		// Read the header of the array file
		iRet = fscanf(fp, "%ld", &Ntot);
		assert(iRet > 0);
		assert(Ntot == iTipsyNumParticles(in));

		// Read the array
		for (i=0; i<Ntot; i++)
		{
				iRet = fscanf(fp, "%lf", &dVal);
				//if (iRet <= 0 && feof(fp)) break;

				assert(iRet > 0);
				
				// Is dMin <= dVal <= dMax
				if(dMin <= dVal && dVal <= dMax)
				{
						TipsyAddGas(out,&in->gp[i]);
				}

		}
		
		// Write to stdout
        TipsyWriteAll(out,time,NULL);

		// Free memory
		TipsyFinish(in);
		TipsyFinish(out);
}
