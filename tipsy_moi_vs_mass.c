/*
 * Determine the moment of inertia as a function of enclosed mass for a tipsy binary file.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

/* 
 * This function is required to sort particles with qsort. It compares two particles and returns
 * -1 if p1.R < p2.R, 1 if p1.R > p2.R or 0 if they have the same distance form the origin. 
 */
int compare_radius(const void* a, const void* b) {
    struct gas_particle p1 = *(struct gas_particle*) (a);
    struct gas_particle p2 = *(struct gas_particle*) (b);
    double R1 = 0.0;
    double R2 = 0.0;
    int i;

    for (i=0; i<3; i++) {
        R1 += p1.pos[i]*p1.pos[i];
        R2 += p2.pos[i]*p2.pos[i];
    }

    R1 = sqrt(R1);
    R2 = sqrt(R2);

    if (R1 < R2) return -1;
    if (R1 > R2) return 1;
    return 0;
}

/*
 * Sort all particles according to their distance from the origin.
 */
void TipsyRadialSort(TCTX in) {
    int N;
    
	N = iTipsyNumParticles(in);

    /* Now sort the particles according to radius */
    qsort(in->gp, N, sizeof(struct gas_particle), compare_radius);
}

double CalcMoI(TCTX in, double dDesiredMass, double *pEncMass, double *pRadius) {
    double M = 0.0;
    double I = 0.0;
    double R;
    int N;
    int i, j;

	N = iTipsyNumParticles(in);

    assert(N == in->nGas);

    assert(pEncMass != NULL);
    assert(pRadius != NULL);

    *pRadius = 0.0;

    for (i=0; i<N; i++) {
        M += in->gp[i].mass;

        if (M > dDesiredMass) {
            M -= in->gp[i].mass;
            break;
        }
        
        R = 0.0;
        for (j=0; j<3; j++) {
            R += (in->gp[i].pos[j]*in->gp[i].pos[j]);
        }
        
        R = sqrt(R);

        assert(*pRadius <= R);

        *pRadius = R;

        // Assume rotation around the z-axis.
        I += (in->gp[i].pos[0]*in->gp[i].pos[0]+in->gp[i].pos[0]*in->gp[i].pos[0])*in->gp[i].mass;
    }
    
    *pEncMass = M;
    return I;
}

double CalcTotalMass(TCTX in) {
    double M = 0.0;
    int N;
    int i;

	N = iTipsyNumParticles(in);

    assert(N == in->nGas);

    for (i=0; i<N; i++) {
        M += in->gp[i].mass;
    }

    return M;
}

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    double dMinMass, dMaxMass;
    double dMinMassFrac;
    double I;
    double M, dM;
    double R;
    int nSteps;
    // The mass unit in cgs
    double Munit = 9.5607162e25;
    // Earth's mass in cgs
    double MEarth = 5.98e27;
    int i;

    // Check command line arguments
    if (argc != 3) {
        fprintf(stderr,"Usage: tipsy_moi_vs_mass <dMinMassFrac> <nSteps> <input.std\n");
        exit(1);
    }

    dMinMassFrac = atof(argv[1]);
    assert((dMinMassFrac >= 0.0) && (dMinMassFrac <= 1.0));

    nSteps = atoi(argv[2]);
    assert(nSteps > 0.0);

    // Read all particles
    TipsyInitialize(&in, 0, "stdin");
    TipsyReadAll(in);

    // Sort the particles
    TipsyRadialSort(in);

    dMaxMass = CalcTotalMass(in);
    dMinMass = dMinMassFrac*dMaxMass;

    assert(dMinMass <= dMaxMass);

    dM = (dMaxMass-dMinMass)/(nSteps-1);

    printf("#%14s%15s%15s%15s%7s\n", "Mass", "Radius", "MoI", "norm. MoI", "Frac");
    // Calculate the MoI for different enclosed masses
    for (i=0; i<nSteps; i++) {
        M = dMinMass + i*dM;

        I = CalcMoI(in, M, &M, &R);

        printf("%15.7E%15.7E%15.7E%15.7E%7.4f\n", M, R, I, I/(M*R*R), M/dMaxMass);
    }
    return 0;
}

