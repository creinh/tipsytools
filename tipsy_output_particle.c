/*
 * This program reads a tipsy binary file and prints a particle
 * data to stdout.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in;
		int N, iOrder;
		double dSoft;
		double time, mass;

        int i;

		// Check command line arguments
		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_output_particle <iOrder> <input.std>\n");
			fprintf(stderr,"Note that iOrder starts from 0.\n");
			exit(1);
		}
		
		iOrder = atoi(argv[1]);
		assert(iOrder >= 0);

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");

		N =  iTipsyNumParticles(in);
		assert(iOrder < N);

		// Read all particles
		TipsyReadAll(in);

		/*
		 * Output all particle data:
		 *
		 * Number, iMat, mass, position, velocities, density, internal energy, ...
		 *
		 */
		printf("%10i",iOrder);
		printf("%3i",(int) in->gp[iOrder].metals);
		printf("%15.7E",in->gp[iOrder].mass);

		// Pos
		for (i=0; i<3; i++)
		{
			printf("%15.7E",in->gp[iOrder].pos[i]);

		}

		// Vel
		for (i=0; i<3; i++)
		{
			printf("%15.7E",in->gp[iOrder].vel[i]);

		}

		printf("%15.7E",in->gp[iOrder].rho);
		printf("%15.7E",in->gp[iOrder].temp);

#ifdef PRINT_PRESSURESOUND

#endif
		printf("\n");
		
		// Free memory
		TipsyFinish(in);
}



