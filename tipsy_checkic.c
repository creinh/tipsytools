/*
 * This program reads a Tipsy binary file and checks, if the IC are physical (e.g., no particles
 * are below the cold curve of the EOS).
 *
 * Author: Christian Reinhardt
 * Date:   18.02.2019
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "tillotson/tillotson.h"
#include "tipsy.h"

#define INDEX(i, j) (((i)*granite->nTableV) + (j))

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int N;
    double time;
    // Tillotson library
    TILLMATERIAL **tillMat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    // Initial guesses for the minimal and maximum density and internal energy.
    double rhomin = DBL_MAX;
    double rhomax = 0.0;
    double umin = DBL_MAX;
    double umax = 0.0;
    double vmax = 1200.0;
    int nTableRho = 1000;
    int nTableV = 1000;
    int nMat, iMat;
    int nError = 0;
    int nWarning = 0;
    int iRet, i;

#ifdef EOS_MAT_MAX
    nMat = EOS_MAT_MAX;
#else
    nMat = TILL_N_MATERIAL_MAX;
#endif

    if (argc != 1) {
        fprintf(stderr,"Usage: tipsy_checkic <tipsy.std\n");
        exit(1);
    }

    // Allocate memory
    tillMat = (TILLMATERIAL**) calloc(nMat, sizeof(TILLMATERIAL *));
    fprintf(stderr, "Memory for the Tillotson library is allocated (nMat= %i).\n", nMat);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Read all particles and determine rhomin, rhomax, umin and umax.
    TipsyReadAll(in);
    rhomax = 0.0;

    for (i=0; i<N; i++)
    {
        if (in->gp[i].rho > rhomax) {
            rhomax = in->gp[i].rho;
        }

        if (in->gp[i].rho < rhomin) {
            rhomin = in->gp[i].rho;
        }

        if (in->gp[i].temp > umax) {
            umax = in->gp[i].temp;
        }

        if (in->gp[i].temp < umin) {
            umin = in->gp[i].temp;
        }
    }

    fprintf(stderr, "N= %i t= %g\n", N, time);
    fprintf(stderr, "rho_min= %g rho_max= %g u_min= %g u_max= %g\n", rhomin, rhomax, umin, umax);

    rhomin *= 0.9;
    rhomax *= 1.5;

    // Use the same values as Gasoline
    rhomin = 1e-4;
    rhomax = 100.0;

    // Now check, if all the particles are in a physical state.
    for (i=0; i<N; i++)
    {
        iMat = (int) in->gp[i].metals;

        // Initialize the material needed.
        if (tillMat[iMat] == NULL)
        {
            fprintf(stderr, "Particle %i: iMat %i not yet initialized.\n", i, iMat);
            tillMat[iMat] = tillInitMaterial(iMat, dKpcUnit, dMsolUnit);
            assert(tillMat[iMat] != NULL);
            tillInitLookup(tillMat[iMat], nTableRho, nTableV, rhomin, rhomax, vmax);
            assert(tillMat[iMat]->Lookup != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        if (tillMat[iMat]->iMaterial != MAT_IDEALGAS) {

            iRet = tillIsInTable(tillMat[iMat], in->gp[i].rho, in->gp[i].temp);

            switch(iRet)
            {
                case TILL_LOOKUP_OUTSIDE_RHOMIN:
                    fprintf(stderr, "Warning: Particle %i is below the minimum density (rho= %g).\n",
                            i, in->gp[i].rho);
                    nWarning++;
                    break;
                case TILL_LOOKUP_OUTSIDE_RHOMAX:
                    fprintf(stderr, "Warning: Particle %i is above the maximum density (rho= %g).\n",
                            i, in->gp[i].rho);
                    nWarning++;
                    break;
                case TILL_LOOKUP_OUTSIDE_VMIN:
                    fprintf(stderr, "Error: Particle %i is below the cold curve (rho= %g, u= %g).\n",
                            i, in->gp[i].rho, in->gp[i].temp);
                    nError++;
                    break;
                case TILL_LOOKUP_OUTSIDE_VMAX:
                    fprintf(stderr, "Warning: Particle %i is above vmax (rho= %g, u= %g).\n", i,
                            in->gp[i].rho, in->gp[i].temp);
                    nWarning++;
                    break;
            }
        }
    }

    printf("IC: nError= %i nWarning= %i\n", nError, nWarning);

    TipsyFinish(in);

    if ((nError > 0) || (nWarning > 0)) 
        return 1;

    return 0;
}
