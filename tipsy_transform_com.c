/*
 ** This program reads a tipsy binary file and transforms the particles
 ** to positions and velocities to the COM frame.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

//#define DOUBLE_POS

struct tipsyMarkData {
    int nTot;
    int nGas;
    int nStar;
    int nMark;
    int *iOrder;
};

struct tipsyMarkData *tipsyReadMarkFile(char *Filename) {
    struct tipsyMarkData *data;
    FILE *fp;
    int iRet;

    // Allocate memory and open the mark file
    data = calloc(1, sizeof(struct tipsyMarkData));
    
    if (data == NULL)
        return data;

    fp = fopen(Filename, "r");
    
    if (fp == NULL) {
        free(data);
        return NULL;
    }

    // Read the header
    iRet = fscanf(fp, "%d %d %d", &data->nTot, &data->nGas, &data->nStar);

    if (iRet != 3) {
        free(data);
        return NULL;
    }

//  printf("%i %i %i\n", data->nTot, data->nGas, data->nStar);

    // Allocate memory for the marked particles
    data->iOrder = calloc(data->nTot, sizeof(int));

    if (data->iOrder == NULL) {
        free(data);
        return NULL;
    }

    data->nMark = 0;

//    fprintf(stderr, "Reading mark file: nMark= %i\n", data->nMark);

    /*
     * Read all marked particles (note that this code does not realize if the
     * format of the mark file is wrong!).
     */
    while (fscanf(fp, "%d", &data->iOrder[data->nMark]) == 1) {
//        printf("%i\n", data->iOrder[data->nMark]);
//        fprintf(stderr, "iOrder= %i, nMark= %i\n", data->iOrder[data->nMark], data->nMark);
        data->nMark++;
    }

    fclose(fp);

    return data;
}

int bIsParticleMarked(struct tipsyMarkData *data, int iOrder) {
    int bMarked;
    int i;

    bMarked = FALSE;

    for (i=0; i<data->nMark; i++) {
        if (data->iOrder[i] == iOrder) {
            bMarked = TRUE;
            return bMarked;
        }
    }

    return bMarked;
}


void Tipsy_Calc_COM(TCTX in, double *pcom, double *pvcom, double *pMass, struct tipsyMarkData *pMark) {
    double ipMass;
    int N,i,j;

    assert(in != NULL);
    assert(pcom != NULL);
    assert(pvcom != NULL);

    N = iTipsyNumParticles(in);

    // Calculate COM
    *pMass = 0.0;

    for (j = 0; j < 3; j++) {
        pcom[j] = 0.0;
        pvcom[j] = 0.0;
    }
    /*
       fprintf(stderr,"COM: ");
       for (j = 0; j < 3; j++)
       {
    //			com[j] *= 1.0/mass;
    fprintf(stderr,"%.6e ",com[j]);
    }
    fprintf(stderr," M=%.6e\n",mass);
    */
    for (i = 0; i < N; i++) {
        if ((pMark != NULL) && (!bIsParticleMarked(pMark, i))) continue;

        for (j = 0; j < 3; j++) {
            pcom[j] += in->gp[i].mass*in->gp[i].pos[j];
            pvcom[j] += in->gp[i].mass*in->gp[i].vel[j];
        }
        *pMass += in->gp[i].mass;
    }

    ipMass = *pMass;

    for (j = 0; j < 3; j++) {
        pcom[j] *= 1.0/ipMass;
        pvcom[j] *= 1.0/ipMass;
    }
}

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in,out;
    int N;
    double time, mass;
    double *com,*vcom;
    struct tipsyMarkData *mark;
    int i,j;

    // Check command line arguments
    if (argc == 3) {
        mark = tipsyReadMarkFile(argv[2]);
    } else if (argc == 2) {
        mark = NULL;
    } else {
        fprintf(stderr,"Usage: tipsy_transform_com <input.std> <input.mark>\n");
        fprintf(stderr,"Note: Providing a mark file to calculate the COM is optional.\n");
        exit(1);
    }

    // Allocate memory	
    com = malloc(3*sizeof(double));
    assert(com != NULL);

    vcom = malloc(3*sizeof(double));
    assert(vcom != NULL);

    // Initialize tipsy library
    TipsyInitialize(&in,0,argv[1]);
    //		TipsyInitialize(&in,0,"stdin");
    TipsyInitialize(&out,0,NULL);

    TipsyReadAll(in);

    // Calculate COM
    Tipsy_Calc_COM(in, com, vcom, &mass, mark);

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    fprintf(stderr,"COM: ");
    for (j = 0; j < 3; j++) {
        //			com[j] *= 1.0/mass;
        //			vcom[j] *= 1.0/mass;
        fprintf(stderr,"%.6e ",com[j]);
    }
    fprintf(stderr,"\n");
    fprintf(stderr,"VCOM: ");
    for (j = 0; j < 3; j++) {
        fprintf(stderr,"%.6e ",vcom[j]);
    }
    fprintf(stderr," M=%.6e\n",mass);

    //		fprintf(stderr,"\n");
    //		fprintf(stderr,"Transforming particles to COM reference frame.\n");

    // Read particles from the input file
    for (i = 0; i < N; i++) {
        // R_new = R_old - R_com
        for (j = 0; j < 3; j++) {
            in->gp[i].pos[j] -= com[j];
            in->gp[i].vel[j] -= vcom[j];
        }
        TipsyAddGas(out, &in->gp[i]);
    }

    // Write to stdout
    TipsyWriteAll(out,time,NULL);

    free(com);
    free(vcom);
    TipsyFinish(in);
    TipsyFinish(out);

    return 0;
}



