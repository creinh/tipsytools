/*
 * Author:   Christian Reinhardt
 * Created:  29.12.2022
 * Modified:
 *
 * This program generates a ballic 1D model from the models of Helled & Bodenheimer (2010).
 */
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <EOSlib.h>

/* 1D model generated with ballic. */
struct BallicModel {
    double *r;
    double *rho;
    double *u;
    double *M;
    int *iMat;
    double *P;
    double *T;
    int nTable;
    /* Additional data. */
    double dKpcUnit;
    double dMsolUnit;
    char *chFile;
};

/* 1D model from Ravit's paper (all quantities are in cgs). */
struct RavitModel {
    double *r;
    double *rho;
    double *P;
    double *T;
    double *M;
    int nTable;
    char *chFile;
};

int WriteBallicModel(struct BallicModel *model, FILE *fp) {
    assert(model != NULL);
    assert(fp != NULL);

    /* Header. */
    fprintf(fp, "# Input: %s\n", model->chFile);
    fprintf(fp, "# dKpcUnit=%15.7E dMsolUnit=%15.7E\n", model->dKpcUnit, model->dMsolUnit);
    fprintf(fp, "#\n");
    fprintf(fp, "#%14s%15s%15s%15s%4s%15s%15s\n", "R", "rho", "M", "u", "mat", "P", "T");

    for (int i=0; i<model->nTable; i++) {
        fprintf(fp,"%15.7E%15.7E%15.7E%15.7E%4i%15.7E%15.7E\n", model->r[i], model->rho[i],
                model->M[i], model->u[i], model->iMat[i], model->P[i], model->T[i]);
    }

    return 0;
}


struct RavitModel *ReadRavitModel(char *chFile) {
    struct RavitModel *model;
    FILE *fp;
    char *chLine;
    size_t nCharMax = 256;
    int iRet;
    int nTableMax = 100000;
    int i = 0;

    /* Allocate memory. */
    model = (struct RavitModel *) calloc(1, sizeof(struct RavitModel));
    assert(model != NULL);

    model->r = (double *) calloc(nTableMax, sizeof(double));
    assert(model->r != NULL);
    model->rho = (double *) calloc(nTableMax, sizeof(double));
    assert(model->rho != NULL);
    model->P = (double *) calloc(nTableMax, sizeof(double));
    assert(model->P != NULL);
    model->T = (double *) calloc(nTableMax, sizeof(double));
    assert(model->T != NULL);
    model->M = (double *) calloc(nTableMax, sizeof(double));
    assert(model->M != NULL);
    model->nTable = 0;

    chLine = (char *) calloc(nCharMax, sizeof(char));

    /* Open the file. */
    fp = fopen(chFile, "r");
    assert(fp != NULL);

    while (getline(&chLine, &nCharMax, fp) != -1) {
        /* Check if its a comment. */
        if (strchr(chLine, '#') != NULL) continue;

        iRet = sscanf(chLine, "%lf %lf %lf %lf %lf", &model->r[i], &model->rho[i],
                      &model->P[i], &model->T[i], &model->M[i]);

        /* Check if the number of matches is correct. */
        assert(iRet == 5);
        
        /* Check that the values are sensible. */
        assert(model->r[i] >= 0.0);
        assert(model->rho[i] >= 0.0);
        assert(model->T[i] >= 0.0);
        assert(model->M[i] >= 0.0);

        i++;

        assert(i < nTableMax);
    }

    model->nTable = i;

    fprintf(stderr, "ReadRavitModel: read %i lines.\n", model->nTable);

    //strcpy(model->chFile, chFile);
    model->chFile = chFile;

    return model;
}

int PrintRavitModel(struct RavitModel *model, FILE *fp) {
    assert(model != NULL);

    fprintf(fp,"#%14s%15s%15s%15s%15s\n", "R", "rho", "P", "T", "M");

    for (int i=0; i<model->nTable; i++) {
        fprintf(fp, "%13.3E%11.3E%11.3E%11.3E%11.3E\n", model->r[i], model->rho[i], model->P[i], model->T[i], model->M[i]);
    }

    return 0;
}

struct BallicModel *ConvertModel(struct RavitModel *RavitModel) {
    struct BallicModel *model;
    EOSMATERIAL *Mat;
    double dKpcUnit = 4.84821e-09;
    double dMsolUnit = 9.53869e-04;
    /* 1 au in cm */
    double Lunit = 1.49600e+13;
    /* 1 M_solar in cgs. */
    double Munit = 1.89820e+30;
    /* Use extended SCvH EOS. */
    int iMat = 114;
    
    assert(RavitModel != NULL);

    /* Initialize EOS. */
    Mat = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
    assert(Mat != NULL);

    model = (struct BallicModel *) calloc(1, sizeof(struct BallicModel));
    assert(model != NULL);

    model->r = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->r != NULL);
    model->rho = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->rho != NULL);
    model->u = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->u != NULL);
    model->M = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->M != NULL);
    model->iMat = (int *) calloc(RavitModel->nTable, sizeof(int));
    assert(model->iMat != NULL);
    model->P = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->P != NULL);
    model->T = (double *) calloc(RavitModel->nTable, sizeof(double));
    assert(model->T != NULL);

    model->nTable = RavitModel->nTable;

    for (int i=0; i<model->nTable; i++) {
        double rho = RavitModel->rho[i]/Mat->scvheosmaterial->dGmPerCcUnit;
        double T = RavitModel->T[i];

        model->r[i] = RavitModel->r[i]/Lunit;
        model->rho[i] = rho;
        model->u[i] = EOSUofRhoT(Mat, rho, T);
        model->M[i] = RavitModel->M[i]/Munit;
        model->iMat[i] = iMat;
        model->P[i] = RavitModel->P[i]/(Mat->scvheosmaterial->dErgPerGmUnit*Mat->scvheosmaterial->dGmPerCcUnit);
        model->T[i] = T;
    }

    model->dKpcUnit = dKpcUnit;
    model->dMsolUnit = dMsolUnit;
    model->chFile = RavitModel->chFile;

    return model;
}


int main(int argc, char **argv) {
    struct RavitModel *RavitModel;
    struct BallicModel *model;

    if (argc != 2) {
        fprintf(stderr, "Usage: model_ravit <model_ravit.dat>\n");
        exit(1);
    }

    RavitModel = ReadRavitModel(argv[1]);
    
    //PrintRavitModel(RavitModel, stderr);
    //exit(1);

    model = ConvertModel(RavitModel);

    WriteBallicModel(model, stdout);

    return 0;
}



