/*
 ** This program determines a models radius by
 ** 1) Finding the particle, that is most distant from the COM
 ** 2) Determining all particles volumes and then calculating the radius from
 **    the total volume. 
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void Tipsy_Calc_COM(TCTX in, double *pcom, double *pvcom, double *pMass)
{
		double ipMass;
		int N,i,j;

		// Make sure that the particles have been loaded
		assert(in != NULL);
		
		assert(pcom != NULL);
		assert(pvcom != NULL);
				
		N = iTipsyNumParticles(in);

		// Calculate COM
		*pMass = 0.0;

		for (j = 0; j < 3; j++)
		{
			pcom[j] = 0.0;
			pvcom[j] = 0.0;
		}
/*
		fprintf(stderr,"COM: ");
		for (j = 0; j < 3; j++)
		{
//			com[j] *= 1.0/mass;
			fprintf(stderr,"%.6e ",com[j]);
		}
		fprintf(stderr," M=%.6e\n",mass);
*/
        for (i = 0; i < N; i++)
        {
				for (j = 0; j < 3; j++)
				{
					pcom[j] += in->gp[i].mass*in->gp[i].pos[j];
					pvcom[j] += in->gp[i].mass*in->gp[i].vel[j];
				}
//				fprintf(stderr,"i: %i mass: %g\n",i,in->gp[i].mass);
				*pMass += in->gp[i].mass;
		}
		
		ipMass = *pMass;
//		fprintf(stderr,"ipMass: %g %g\n",ipMass,*pMass);
		for (j = 0; j < 3; j++)
		{
			pcom[j] *= 1.0/ipMass;
			pvcom[j] *= 1.0/ipMass;
		}
}

double CalcRadiusDist(TCTX in, double *pcom, double *pvcom)
{
		double R, Rmax;
		int N;
		int i,j;

		assert(in != NULL);
		assert(pcom != NULL);
		assert(pvcom != NULL);

		N = iTipsyNumParticles(in);
		
		Rmax = 0.0;
        
		for (i = 0; i < N; i++)
        {
				R = 0.0;
			
				for (j = 0; j < 3; j++)
				{
					R += (in->gp[i].pos[j]-pcom[j])*(in->gp[i].pos[j]-pcom[j]);
				}

				if (R > Rmax)
					Rmax = R;
		}

		return(sqrt(Rmax));
}

double CalcRadiusVol(TCTX in)
{
		double V, R;
		int N;
		int i;

		assert(in != NULL);

		N = iTipsyNumParticles(in);
		
		V = 0.0;
        
		for (i = 0; i < N; i++)
        {
				V += in->gp[i].mass/in->gp[i].rho;
		}
		
		R = cbrt(3.0*V/(4.0*M_PI));

//		fprintf(stderr,"V= %g, R= %g\n", V, R);
		
		return(R);
}

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in;
		int N;
		double dSoft;
		double time, mass;
		double *com,*vcom;

        int i,j;

		// Check command line arguments
		if (argc != 1) {
			fprintf(stderr,"Usage: tipsyradiusr <input.std>\n");
			exit(1);
		}
		
		// Allocate memory	
		com = malloc(3*sizeof(double));
		assert(com != NULL);

		vcom = malloc(3*sizeof(double));
		assert(vcom != NULL);

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");

		// Read all particles
		TipsyReadAll(in);

		// Calculate COM
		Tipsy_Calc_COM(in, com, vcom, &mass);
	
		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);

		fprintf(stderr,"COM: ");
		for (j = 0; j < 3; j++)
		{
//			com[j] *= 1.0/mass;
//			vcom[j] *= 1.0/mass;
			fprintf(stderr,"%.6e ",com[j]);
		}
		fprintf(stderr,"\n");
		fprintf(stderr,"VCOM: ");
		for (j = 0; j < 3; j++)
		{
			fprintf(stderr,"%.6e ",vcom[j]);
		}
		fprintf(stderr," M=%.6e\n",mass);

		printf("t= %g  M= %g", time,mass);

		// Determine radius from method 1
		printf("  R= %g", CalcRadiusDist(in, com, vcom));

        // Determine radius from method 2
		printf("  R= %g\n", CalcRadiusVol(in));
		
		free(com);
		free(vcom);
		TipsyFinish(in);
}



