/*
 * This program reads a Tipsy binary file and generates a tipsy array file
 * containing either the density, internal energy, temperature or pressure
 * of the particles.
 *
 * 01.03.2018: Updated the code so it allows different EOS.
 * 02.04.2020: Implemented a general EOS interface
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <stdbool.h>
#include <argp.h>
#include <EOSlib.h>
#include "tipsy.h"

#define TIPSY_QUANTITY_DEN		0
#define TIPSY_QUANTITY_LOGDEN	1
#define TIPSY_QUANTITY_U		2
#define TIPSY_QUANTITY_LOGU		3
#define TIPSY_QUANTITY_TEMP		4
#define TIPSY_QUANTITY_LOGTEMP	5
#define TIPSY_QUANTITY_PRESS	6
#define TIPSY_QUANTITY_LOGPRESS	7
#define TIPSY_QUANTITY_CS		8
#define TIPSY_QUANTITY_MAT		9
#define TIPSY_QUANTITY_MASS		10
#define TIPSY_QUANTITY_ENTROPY	11

/*
 * Parse command line arguments with GNU argp.
 */
const char *argp_program_version = "tipsyarray v1.0";
const char *argp_program_bug_address = "christian.reinhardt@ics.uzh.ch";

/* Description of the program. */
static char doc[] = "Generate array files for a tipsy binary file containing particle data (e.g., density, temperature, pressure).";

/* Description of input arguments. */
static char args_doc[] = "[Quantity]";

/* Define maximum number of arguments. */
#define NUM_ARGS_MAX 100

/* Start from 256 because we do not want short options. */
enum OPTIONS {
    OPT_SET_KPCUNIT = 256,
    OPT_SET_MSOLUNIT,
    OPT_SET_RHOSURF,
    OPT_SET_GAMMA,
    OPT_SET_MEANMOLMASS,
    OPT_SET_TEMP
};

/* Optional arguments. */
static struct argp_option options[] = {
    { "dKpcUnit", OPT_SET_KPCUNIT, "KPCUNIT", 0, "Set length unit in kpc (default: 2.06701e-13)." },
    { "dMsolUnit", OPT_SET_MSOLUNIT, "MSOLUNIT", 0, "Set mass unit in Solar masses (default: 4.80438e-08)." },
    { "dConstGamma", OPT_SET_GAMMA, "GAMMA", 0, "Set gamma for the ideal gas eos (default: 5/3)." },
    { "dMeanMolMass", OPT_SET_MEANMOLMASS, "MEANMOLMASS", 0, "Set mean molecular mass in units of m_H (default: 1.0)." },
    { "bTemp", OPT_SET_TEMP, 0, 0, "Use temperature as input (as required by pkdgrav3)." },
    { 0 }
};

struct arguments {
    /* Required arguments. */
    char *args[NUM_ARGS_MAX];
    int nArgs;
    /* Options. */
    double dKpcUnit;
    double dMsolUnit;
    bool bKpcUnitSet;
    bool bMsolUnitSet;
    double dConstGamma;
    double dMeanMolMass;
    bool bTemp;
};

error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
        case OPT_SET_KPCUNIT:
            arguments->bKpcUnitSet = true;
            arguments->dKpcUnit = atof(arg);
            break;
        case OPT_SET_MSOLUNIT:
            arguments->bMsolUnitSet = true;
            arguments->dMsolUnit = atof(arg);
            break;
        case OPT_SET_GAMMA:
            arguments->dConstGamma = atof(arg);
            break;
        case OPT_SET_MEANMOLMASS:
            arguments->dMeanMolMass = atof(arg);
            break;
        case OPT_SET_TEMP:
            arguments->bTemp = true;
            break;
        case ARGP_KEY_ARG:
            /* Too many arguments. */
            if (state->arg_num >= arguments->nArgs) {
                argp_usage(state);
            }
            arguments->args[state->arg_num] = arg;    
            break;
        case ARGP_KEY_END:
            if (state->arg_num < arguments->nArgs) {
                /* Not enough arguments. */
                argp_usage(state);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int type, N;
    double dSoft;
    struct base_particle *p;
    struct gas_particle *g;
    // EOS library
    EOSMATERIAL **Mat;
    double dKpcUnit;
    double dMsolUnit;
    int iMat;
    int nMat = EOS_N_MATERIAL_MAX;
    bool bEOSLib;
    bool bEntropyLookup;
    int iQuantity;
    struct arguments arguments;
    bool bTemp;
    struct igeosParam igParam;

    /* Set number of required arguments. */
    arguments.nArgs = 1;

    /* Initialize default arguments. */
    arguments.dKpcUnit = 2.06701e-13;
    arguments.dMsolUnit = 4.80438e-08;
    arguments.bKpcUnitSet = false;
    arguments.bMsolUnitSet = false;
    arguments.dConstGamma = 5.0/3.0;
    arguments.dMeanMolMass = 1.0;
    arguments.bTemp = false;
    
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (arguments.bKpcUnitSet != arguments.bMsolUnitSet) {
        fprintf(stderr, "Error: only one unit (dKpcUnit or dMsolUnit) was specified by the user.\n");
        exit(1);
    }

    dKpcUnit = arguments.dKpcUnit;
    dMsolUnit = arguments.dMsolUnit;
    
    /* Parameters for ideal gas. */    
    igParam.dConstGamma = arguments.dConstGamma;
    igParam.dMeanMolMass = arguments.dMeanMolMass;

    bTemp = arguments.bTemp;

    // Print version information
    fprintf(stderr,"EOS library version: %s\n", EOS_VERSION_TEXT);

    // We assume that the EOS library is not needed
    bEOSLib = false;

    // We assume that the entropy lookup table is not needed
    bEntropyLookup = false;

    // Check which quantity should be calculated 
    if (strcmp(arguments.args[0], "density") == 0) {
        iQuantity = TIPSY_QUANTITY_DEN;
    } else if (strcmp(arguments.args[0], "logdensity") == 0) {
        iQuantity = TIPSY_QUANTITY_LOGDEN;
    } else if (strcmp(arguments.args[0], "energy") == 0) {
        iQuantity = TIPSY_QUANTITY_U;
        if (bTemp) {
            bEOSLib = 1;
            bEntropyLookup = 1;
        }
    } else if (strcmp(arguments.args[0], "logenergy") == 0) {
        iQuantity = TIPSY_QUANTITY_LOGU;
        if (bTemp) {
            bEOSLib = 1;
            bEntropyLookup = 1;
        }
    } else if (strcmp(arguments.args[0], "temp") == 0) {
        iQuantity = TIPSY_QUANTITY_TEMP;
        if (!bTemp) {
            bEOSLib = 1;
            bEntropyLookup = 1;
        }
    } else if (strcmp(arguments.args[0], "logtemp") == 0) {
        iQuantity = TIPSY_QUANTITY_LOGTEMP;
        if (!bTemp) {
            bEOSLib = 1;
            bEntropyLookup = 1;
        }
    } else if (strcmp(arguments.args[0], "pressure") == 0) {
        iQuantity = TIPSY_QUANTITY_PRESS;
        bEOSLib = 1;
    } else if (strcmp(arguments.args[0], "logpressure") == 0) {
        iQuantity = TIPSY_QUANTITY_LOGPRESS;
        bEOSLib = 1;
    } else if (strcmp(arguments.args[0], "soundspeed") == 0) {
        iQuantity = TIPSY_QUANTITY_CS;
        bEOSLib = 1;
    } else if (strcmp(arguments.args[0], "mat") == 0) {
        iQuantity = TIPSY_QUANTITY_MAT;
    } else if (strcmp(argv[1], "mass") == 0) {
        iQuantity = TIPSY_QUANTITY_MASS;
    } else if (strcmp(arguments.args[0], "entropy") == 0) {
        iQuantity = TIPSY_QUANTITY_ENTROPY;
        bEOSLib = 1;
        bEntropyLookup = 1;
    } else {
        fprintf(stderr,"Unknown quantity: %s\n", arguments.args[0]);
        fprintf(stderr,"\n");
        fprintf(stderr,"iQuantity can be:\n");
        fprintf(stderr,"<density>, <logdensity>, <energy>, <logenergy>,\n");
        fprintf(stderr,"<temp>, <logtemp>, <pressure>, <logpressure> or <mat>\n");
        exit(1);
    }

    // Make sure that the EOS library is initialized too if the entropy lookup table is needed
    if (bEntropyLookup) bEOSLib = true;

    if (bEOSLib)
    {
        // Allocate memory for the array that containts all materials in the EOS library
        Mat = (EOSMATERIAL**) calloc(nMat, sizeof(EOSMATERIAL *));
        fprintf(stderr, "Memory for the EOS library is allocated (nMat= %i).\n", nMat);
        fprintf(stderr, "dKpcUnit= %.8e dMsolUnit= %.8e\n", dKpcUnit, dMsolUnit);
        fprintf(stderr, "\n");
    }

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);

    // Print header
    printf("%i\n",N);

    // Read particles from the input file
    for (int i=0; i<N; i++)
    {
        p = pTipsyRead(in,&type,&dSoft);
        assert(type == TIPSY_TYPE_GAS);
        g = (struct gas_particle *)p;

#if 0
        /*
         * Check that a particles density is larger than TILL_RHO_MIN.
         */
        if (g->rho < TILL_RHO_MIN)
            fprintf(stderr, "Particle %i: rho= %g is smaller than TILL_RHO_MIN.\n", i, g->rho);
#endif
        iMat = (int) g->metals;

        /*
         * Initialize the material and look up table if needed.
         */
        if (bEOSLib && Mat[iMat] == NULL)
        {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);

            if (iMat == EOSIDEALGAS) {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, &igParam);
            } else {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
            }
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        if (bEntropyLookup && Mat[iMat]->bEntropyTableInit == EOS_FALSE)
        {
            if (iMat != MAT_IDEALGAS)
            {
                fprintf(stderr, "iMat %i entropy lookup table not yet initialized.\n", iMat);
                EOSinitIsentropicLookup(Mat[iMat], NULL);
                fprintf(stderr, "\n");
            }
        }

        switch(iQuantity)
        {
            case TIPSY_QUANTITY_DEN:
                // Output density
                printf("%.8g\n", g->rho);
                break;

            case TIPSY_QUANTITY_LOGDEN:
                printf("%.8g\n", log(g->rho));
                break;

            case TIPSY_QUANTITY_U:
                // Output internal energy
                if (bTemp) {
                    printf("%.8g\n", EOSUofRhoT(Mat[iMat], g->rho, g->temp));
                } else {
                    printf("%.8g\n", g->temp);
                }
                break;

            case TIPSY_QUANTITY_LOGU:
                // Output internal energy
                if (bTemp) {
                    printf("%.8g\n", log(EOSUofRhoT(Mat[iMat], g->rho, g->temp)));
                } else {
                    printf("%.8g\n", log(g->temp));
                }
                break;

            case TIPSY_QUANTITY_TEMP:
                // Convert internal energy to temperature
                if (bTemp) {
                    printf("%.8g\n", g->temp);
                } else {
                    printf("%.8g\n", EOSTofRhoU(Mat[iMat], g->rho, g->temp));
                }
                break;

            case TIPSY_QUANTITY_LOGTEMP:
                // Convert internal energy to temperature
                if (bTemp) {
                    printf("%.8g\n", log(g->temp));
                } else {
                    printf("%.8g\n", log(EOSTofRhoU(Mat[iMat], g->rho, g->temp)));
                }
                break;

            case TIPSY_QUANTITY_PRESS:
                // Calculate pressure
                if (bTemp) {
                    printf("%.8g\n", EOSPofRhoT(Mat[iMat], g->rho, g->temp));
                } else {
                    printf("%.8g\n", EOSPofRhoU(Mat[iMat], g->rho, g->temp));
                }
                break;

            case TIPSY_QUANTITY_LOGPRESS:
                // Calculate pressure
                if (bTemp) {
                    printf("%.8g\n", log(EOSPofRhoT(Mat[iMat], g->rho, g->temp)));
                } else {
                    printf("%.8g\n", log(EOSPofRhoU(Mat[iMat], g->rho, g->temp)));
                }
                break;

            case TIPSY_QUANTITY_CS:
                // Calculate sound speed
                if (bTemp) {
                    printf("%.8g\n", EOSCofRhoU(Mat[iMat], g->rho, EOSUofRhoT(Mat[iMat], g->rho, g->temp)));
                } else {
                    printf("%.8g\n", EOSCofRhoU(Mat[iMat], g->rho, g->temp));
                }
                break;

            case TIPSY_QUANTITY_MASS:
                // Output particle mass
                printf("%.8g\n", g->mass);
                break;

            case TIPSY_QUANTITY_MAT:
                // Output material
                printf("%i\n", (int) g->metals);
                break;

            case TIPSY_QUANTITY_ENTROPY:
                // Calculate entropy
                if (bTemp) {
                    printf("%.8g\n", log(EOSSofRhoT(Mat[iMat], g->rho, g->temp)));
                } else {
                    printf("%.8g\n", log(EOSSofRhoU(Mat[iMat], g->rho, g->temp)));
                }
                break;

            default:
                // Unknown quantity
                assert(0);
        }
    }

    if (bEOSLib)
    {
        for (int i=0; i<nMat; i++)
        {
            if (Mat[i] != NULL)
                EOSfinalizeMaterial(Mat[i]);
        }
    }

    TipsyFinish(in);

    return 0;
}
