#!/usr/bin/env python2
"""
This program is used to test tipsyprofile.c.

It generates a profile from an existing binary file and compares it to results
obtained using the old code.
"""
from matplotlib import *
from matplotlib.pyplot import *
from numpy import *
from sys import *

def read_model(filename="ballic.model"):
    """
    Read an equilibrium model produced with ballic.

    Paramters:
    ----------
    filename:   Filename (e.g., ballic.model)

    Return:
    -------
    R:          Radius
    rho:        Density
    M:          Enclosed mass
    u:          Internal energy
    mat:        Material ID
    P:          Pressure
    T:          Temperature
    """
    data = loadtxt(filename)

    R   = data[:, 0]
    rho = data[:, 1]
    M   = data[:, 2]
    u   = data[:, 3]
    mat = data[:, 4]
    P   = data[:, 5]
    T   = data[:, 6]

    return R, rho, M, u, mat, P, T


def read_profile(filename="ballic.profile"):
    """
    Read a radial profile of an equilibrium model produced with ballic.

    Paramters:
    ----------
    filename:   Filename (e.g., ballic.profile)

    Return:
    -------
    R:          Radius
    rho:        Density
    u:          Internal energy
    M:          Enclosed mass

    P:          Pressure        (new)
    T:          Temperature     (new)
    mat:        Material ID     (new)
    """
    data = loadtxt(filename)

    R   = data[:, 0]
    rho = data[:, 1]
    u   = data[:, 2]
    M   = data[:, 3]

    # Check if its the next extended format
    if len(data[0,:]) == 7:
        P   = data[:, 4]
        T   = data[:, 5]
        mat = data[:, 6]
        return R, rho, u, M, P, T, mat
    else:
        return R, rho, u, M


"""
Setup the plot.
"""
# Set a font
rcParams['font.family'] = 'serif'
rcParams['font.size'] = 10.0

# Legend
# mpl.rcParams['legend.handlelength']  = 2.9
rcParams['legend.handlelength']  = 0.5
rcParams['legend.frameon']       = False
rcParams['legend.numpoints']     = 1
rcParams['legend.scatterpoints'] = 1

# Adjust axes line width
rcParams['axes.linewidth']   = 0.5

# Adjust ticks
rcParams['xtick.major.size'] = 4
rcParams['xtick.minor.size'] = 2
rcParams['ytick.major.size'] = 4
rcParams['ytick.minor.size'] = 2

# Adjust Font Size
rcParams['xtick.labelsize']  = 'x-small'
rcParams['ytick.labelsize']  = 'x-small'
rcParams['axes.labelsize']   = 'small'

# Set Up Figure, Single Column MNRAS
fig = gcf()
ax = gca()
fig, ax = subplots(1,1)
fig.set_size_inches(8.27*0.39,8.27*(6./8.)*0.39)

"""
if len(argv) != 3:
    print "Usage: profile-plot.py <profile> <model>"
    exit(1)

profile = argv[1]
model = argv[2]

print "Loading:", profile
"""
profile1 = "tipsyprofile.new.profile"
profile2 = "tipsyprofile.old.profile"
model    = "uranus1-impactor2ME-atm-ig-mu1.model"

"""
Plot rho(R).
"""
print "Plotting", profile1
R1, rho1, u1, M1, P1, T1, mat1 = read_profile(profile1)
scatter(R1, rho1, s=1, color='blue')

print "Plotting", profile2
R2, rho2, u2, M2 = read_profile(profile2)
scatter(R1, rho1, s=1, color='green')

print "Plotting", model
R_model, rho_model, M_model, u_model, mat_model, P_model, T_model = read_model(model) 
plot(R_model, rho_model, color='red')

R_min = min(numpy.concatenate((R1, R2, R_model)))
R_max = max(numpy.concatenate((R1, R2, R_model)))

rho_min = min(numpy.concatenate((rho1, rho2, rho_model)))
rho_max = max(numpy.concatenate((rho1, rho2, rho_model)))

u_min = min(numpy.concatenate((u1, u2, u_model)))
u_max = max(numpy.concatenate((u1, u2, u_model)))

M_min = min(numpy.concatenate((M1, M2, M_model)))
M_max = max(numpy.concatenate((M1, M2, M_model)))

xlim(R_min, R_max*1.05)
ylim(rho_min, rho_max*1.05)

xlabel(r'Radius [$R_{\oplus}$]')
ylabel(r'Density [code units]')
savefig('tipsyprofile.density.png', dpi=300, bbox_inches='tight')

clf()

"""
Plot u(R).
"""
scatter(R1, u1, s=1, color='blue')
scatter(R1, u1, s=1, color='green')
plot(R_model, u_model, color='red')

xlim(R_min, R_max*1.05)
ylim(u_min, u_max*1.05)

xlabel(r'Radius [$R_{\oplus}$]')
ylabel(r'Int. energy [code units]')
savefig('tipsyprofile.u.png', dpi=300, bbox_inches='tight')

clf()

"""
Plot M(R).
"""
scatter(R1, M1, s=1, color='blue')
scatter(R1, M1, s=1, color='green')
plot(R_model, M_model, color='red')

xlim(R_min, R_max*1.05)
ylim(M_min, M_max*1.05)

xlabel(r'Radius [$R_{\oplus}$]')
ylabel(r'Mass [code units]')
savefig('tipsyprofile.mass.png', dpi=300, bbox_inches='tight')

clf()


print where(abs(R1-R2) > 1e-3)
print where(abs(rho1-rho2) > 1e-3)
print where(abs(u1-u2) > 1e-3)
print where(abs(M1-M2) > 1e-3)

