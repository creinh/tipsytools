/*
 * This program reads a Tipsy binary file and computes the total energy of all particles.
 *
 * Author: Christian Reinhardt
 * Date:   18.05.2019
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <float.h>
#include "tillotson/tillotson.h"
#include "tipsy.h"

/*
 * Calculate the gravitational potential energy for a particle distribution.
 */
double dGravPot(TCTX in, int iParticle, double eps) {
    double dEpot = 0.0; 
    double r2 = 0.0;
    int N;
    int i, j;

    N = iTipsyNumParticles(in);

    for (i=0; i<N; i++) {
        if (i != iParticle) {
            r2 = 0.0;
            for (j=0; j<3; j++) {
                r2 += (in->gp[iParticle].pos[j]-in->gp[i].pos[j])*
                    (in->gp[iParticle].pos[j]-in->gp[i].pos[j]);
            }

            assert(r2>0.0);

            // Do a Plummer softening
            dEpot += -in->gp[i].mass/sqrt(r2 + eps*eps);
        }
    }

    // Note that G=1 in our code units
    dEpot *= in->gp[iParticle].mass;

    return dEpot;
}


int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int N;
    double time;
    // Tillotson library
    TILLMATERIAL **tillMat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    // Initial guesses for the minimal and maximum density and internal energy.
    double rhomin = DBL_MAX;
    double rhomax = 0.0;
    double umin = DBL_MAX;
    double umax = 0.0;
    double vmax = 1200.0;
    int nTableRho = 1000;
    int nTableV = 1000;
    int nMat, iMat;
    // Total energy
    double dEtot = 0.0;
    // Kinetic energy
    double dEkinTot = 0.0;
    double dEkin = 0.0;
    // Internal energy
    double dUtot = 0.0;
    double dU = 0.0;
    // Potential energy
    double dEpotTot = 0.0;
    double dEpot = 0.0;
    double v2 = 0.0;
    // Gravitational softening
    double eps = 0.0;
    int i, j;

#ifdef EOS_MAT_MAX
    nMat = EOS_MAT_MAX;
#else
    nMat = TILL_N_MATERIAL_MAX;
#endif

    if (argc != 2) {
        fprintf(stderr,"Usage: tipsy_etot eps <tipsy.std\n");
        exit(1);
    }

    eps = atof(argv[1]);
    assert(eps >= 0.0);

    // Allocate memory
    tillMat = (TILLMATERIAL**) calloc(nMat, sizeof(TILLMATERIAL *));
    fprintf(stderr, "Memory for the Tillotson library is allocated (nMat= %i).\n", nMat);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Read all particles and determine rhomin, rhomax, umin and umax.
    TipsyReadAll(in);
    rhomax = 0.0;

    for (i=0; i<N; i++)
    {
        if (in->gp[i].rho > rhomax) {
            rhomax = in->gp[i].rho;
        }

        if (in->gp[i].rho < rhomin) {
            rhomin = in->gp[i].rho;
        }

        if (in->gp[i].temp > umax) {
            umax = in->gp[i].temp;
        }

        if (in->gp[i].temp < umin) {
            umin = in->gp[i].temp;
        }
    }

    fprintf(stderr, "N= %i t= %g\n", N, time);
    fprintf(stderr, "rho_min= %g rho_max= %g u_min= %g u_max= %g\n", rhomin, rhomax, umin, umax);

    rhomin *= 0.9;
    rhomax *= 1.5;

    // Use the same values as Gasoline
    rhomin = 1e-4;
    rhomax = 100.0;

    // Calculate the total energy of all particles.
    for (i=0; i<N; i++)
    {
#if 0
        iMat = (int) in->gp[i].metals;

        // Initialize the material needed.
        if (tillMat[iMat] == NULL)
        {
            fprintf(stderr, "Particle %i: iMat %i not yet initialized.\n", i, iMat);
            tillMat[iMat] = tillInitMaterial(iMat, dKpcUnit, dMsolUnit);
            assert(tillMat[iMat] != NULL);
            tillInitLookup(tillMat[iMat], nTableRho, nTableV, rhomin, rhomax, vmax);
            assert(tillMat[iMat]->Lookup != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }
#endif
        // Kinetic energy
        v2 = 0.0;

        for (j=0; j<3; j++) {
            v2 += in->gp[i].vel[j]*in->gp[i].vel[j];
        }
        dEkin = 0.5*in->gp[i].mass*v2;

        // Internal energy
        dU = in->gp[i].mass*in->gp[i].temp;

        // Potential energy
        dEpot = dGravPot(in, i, eps);

        dEtot += dEkin + dU + dEpot;
        dEkinTot += dEkin;
        dUtot += dU;
        dEpotTot += dEpot;
    }

    printf("# N= %i t= %g eps= %g\n", N, time, eps);
    //printf("#%15s%15s%15s\n", "dEtot", "dEkinTot", "dUtot");
    printf("#          time");
    printf("           Etot");
    printf("           Ekin");
    printf("              U");
    printf("           Epot\n");
    printf("%15.7E%15.7E%15.7E%15.7E%15.7E\n", time, dEtot, dEkinTot, dUtot, dEpotTot);

    TipsyFinish(in);

    return 0;
}
