/*
** This program reads a Tipsy binary file adds a solid body rotation around the z-axis and then rotates the model using euler angles
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

int main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		double omega, phi, theta;
		int i;

		if (argc != 4) {
			fprintf(stderr,"Usage: tipsy_add_angular_velocity_and_rotate <omega> <phi> <theta> < tipsy.std > tipsy_new.std\n");
			exit(1);
		}

        omega = atof(argv[1]);
        phi = atof(argv[2]);
        theta = atof(argv[3]);

        double cphi = cos(phi);
        double sphi = sin(phi);
        double ctheta = cos(theta);
        double stheta = sin(theta);
        double cpsi = 1.0;
        double spsi = 0.0;
        
        double a11 = cpsi * cphi - ctheta * sphi * spsi;
        double a21 = cpsi * sphi + ctheta * cphi * spsi;
        double a31 = spsi * stheta;
        double a12 = -spsi * cphi - ctheta * sphi * cpsi;
        double a22 = -spsi * sphi + ctheta * cphi * cpsi;
        double a32 = cpsi * stheta;
        double a13 = stheta * sphi;
        double a23 = -stheta * cphi;
        double a33 = ctheta;

        double x_new, y_new, z_new, vx_new, vy_new, vz_new;

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		// Read particles from the input file
	        for (i = 0; i < N; i++)
        	{
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;

            g->vel[0] += -omega * g->pos[1];
            g->vel[1] += omega * g->pos[0];

            x_new = g->pos[0] * a11 + g->pos[1] * a12 + g->pos[2] * a13;
            y_new = g->pos[0] * a21 + g->pos[1] * a22 + g->pos[2] * a23;
            z_new = g->pos[0] * a31 + g->pos[1] * a32 + g->pos[2] * a33;

            vx_new = g->vel[0] * a11 + g->vel[1] * a12 + g->vel[2] * a13;
            vy_new = g->vel[0] * a21 + g->vel[1] * a22 + g->vel[2] * a23;
            vz_new = g->vel[0] * a31 + g->vel[1] * a32 + g->vel[2] * a33;

            g->pos[0] = x_new;
            g->pos[1] = y_new;
            g->pos[2] = z_new;

            g->vel[0] = vx_new;
            g->vel[1] = vy_new;
            g->vel[2] = vz_new;

			TipsyAddGas(out,g);
		}

		// Write to stdout
 		TipsyWriteAll(out,time,NULL);

		fprintf(stderr,"t=%g\n",time);
		TipsyFinish(in);
		TipsyFinish(out);
		return 0;
}
