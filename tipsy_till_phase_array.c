/*
 * Generate a tipsy array file that contains each particle's phase.
 *
 * Author:   Christian Reinhardt
 * Created:  09.12.2020
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "../tillotson/tillotson.h"
#include "tipsy.h"

int main(int argc, char **argv) {
    // Tipsy library
	TCTX in;
	int N;
	double time;
    // Tillotson library
    TILLMATERIAL **Mat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    int iMat;
    int nMat;
	double rho, u;
	int i;

    nMat = TILL_N_MATERIAL_MAX;

    if (argc != 1) {
        fprintf(stderr,"Usage: tipsy_till_phase_array <tipsy.std\n");
        exit(1);
    }

    // Print version information
    fprintf(stderr,"Tillotson EOS library version: %s\n", TILL_VERSION_TEXT);

    Mat = (TILLMATERIAL**) calloc(nMat, sizeof(TILLMATERIAL *));
    fprintf(stderr, "Memory for the Tillotson EOS library is allocated (nMat= %i).\n", nMat);
    
    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Read all particles
    TipsyReadAll(in);

    // Print header
    printf("%i\n", N);

    // Read particles from the input file
    for (i = 0; i<N; i++) {
        rho = in->gp[i].rho;
		u = in->gp[i].temp;
		iMat = (int) in->gp[i].metals;

        // Initialize the material and look up table if needed.
        if (Mat[iMat] == NULL) {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);
            Mat[iMat] = tillInitMaterial(iMat, dKpcUnit, dMsolUnit);
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        printf("%i\n", tillPhase(Mat[iMat], rho, u));
    }
    

    for (i=0; i<nMat; i++) {
        if (Mat[i] != NULL)
            tillFinalizeMaterial(Mat[i]);
    }

    TipsyFinish(in);
}
