/*
** The header file for model.c.
*/
#ifndef MODEL_HINCLUDED
#define MODEL_HINCLUDED
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>

#define INDEX(i, j) (((i)*granite->nTableV) + (j))

typedef struct model_ctx {
	int iMat;
	/*
	** The lookup table for the equilibrium model.
	*/
	int nTableMax;
	int nTable;

	double *M;
	double *rho;
	double *u;
	double *r;
//	int *mat;
//	double dr;
	double R;
	double Mtot;
	} MODEL;

/* Functions provided by model.c */
MODEL *modelInit();
void modelRead(MODEL *model, char *file);
int riLookup(MODEL *model,double r);
double RMLookup(MODEL *model,double M);
double MLookup(MODEL *model,double r);
double rhoLookup(MODEL *model,double r);
double uLookup(MODEL *model,double r);
#endif
