/*
** This file provides basic functions to read an equilibrium model from a file and look up quantities.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "model.h"

MODEL *modelInit() {
    /* Initialize the model */
	MODEL *model;
    
    model = malloc(sizeof(MODEL));
    assert(model != NULL);

//	model->dKpcUnit = 2.06701e-13;
//	model->dMsolUnit = 4.80438e-08;

	/* Calculate the conversion factors. */
    model->nTableMax = 10000; 
    model->M = malloc(model->nTableMax*sizeof(double));
    assert(model->M != NULL);
    model->rho = malloc(model->nTableMax*sizeof(double));
    assert(model->rho != NULL);
    model->u = malloc(model->nTableMax*sizeof(double));
    assert(model->u != NULL);
    model->r = malloc(model->nTableMax*sizeof(double));
    assert(model->r != NULL);
//	model->mat = malloc(model->nTableMax*sizeof(double));
//	assert(model->mat != NULL);
    model->R = 0.0;
	model->Mtot = 0.0;
    model->nTable = 0;
    
    return(model);
    }

/*
** Read a text file containing an equilibrium model.
*/
void modelRead(MODEL *model, char *file) {
	FILE *fp;
	int iRet, i;

	assert(model != NULL);

	/* Open the file. */
	fp = fopen(file,"r");
	assert(fp != NULL);

	/* Read the model. */
	for (i=0; i<model->nTableMax;i++)
	{

//		fprintf(stderr,"i=%i\n",i);
		// R  M(R)  rho  u  iMat
//		iRet = fscanf(fp, "%lf %lf %lf %lf %d", &model->r[i],&model->M[i],&model->rho[i],&model->u[i],&model->mat[i]);
		iRet = fscanf(fp, "%lf %lf %lf %lf", &model->r[i],&model->rho[i],&model->M[i],&model->u[i]);

		if (iRet <= 0 && feof(fp)) break;

		assert(iRet > 0);
		
		assert(model->r[i] >= 0.0);
		assert(model->M[i] >= 0.0);
		assert(model->rho[i] >= 0.0);
		assert(model->u[i] >= 0.0);
//		assert(model->mat[i] >= 0.0);
		
		model->nTable = i;
		//printf("%g  %g  %g  %g  %i\n",model->r[i],model->M[i],model->rho[i],model->u[i],model->mat[i]);
	}

	model->nTable += 1;
	model->R = model->r[model->nTable-1];
	model->Mtot = model->M[model->nTable-1];

	fprintf(stderr,"nTable=%i\n",model->nTable);
	fclose(fp);
}

/*
** Do bisection to find the values r_i and r_i+1 that
** bracket r in the lookup table. The function returns
** i.
*/
int riLookup(MODEL *model,double r) {
	int iLower,iUpper,i;

	iLower = 0;
	iUpper = model->nTable-1;

//	fprintf(stderr,"rLookup: r=%g\n", r);
	/* Make sure that r is in the lookup table. */
	assert(r >= model->r[iLower]);
	assert(r <= model->r[iUpper]);

	assert(model->r[iLower] < model->r[iUpper]);

	/* Do bisection. */
	while (iUpper-iLower > 1)
	{
		i = (iUpper + iLower) >> 1;

		if (r >= model->r[i])
		{
			iLower = i;
		} else {
			iUpper = i;
		}
	}
	
	if (r == model->r[0]) return 0;
	if (r == model->r[model->nTable-1]) return 0;

	return iLower;
}

/*
** Do bisection to find the values M_i and M_i+1 that
** bracket M in the lookup table. The function returns
** i.
*/
int MiLookup(MODEL *model,double M) {
	int iLower,iUpper,i;

	iLower = 0;
	iUpper = model->nTable-1;

	/* Make sure that M is in the lookup table. */
	assert(M >= model->M[iLower]);
	assert(M <= model->M[iUpper]);

	assert(model->M[iLower] < model->M[iUpper]);

	/* Do bisection. */
	while (iUpper-iLower > 1)
	{
		i = (iUpper + iLower) >> 1;

		if (M >= model->M[i])
		{
			iLower = i;
		} else {
			iUpper = i;
		}
	}
	
	if (M == model->r[0]) return 0;
	if (M == model->r[model->nTable-1]) return 0;

	return iLower;
}

/*
** Find the radius for a given enclosed mass.
*/
double RMLookup(MODEL *model,double M) {
	int i;
	double A;
		
	/* If r is outside of the lookup table, return something. */
	i = model->nTable-1;

//	if (M >= model->M[i]) return(model->r[i]);
    if (M >= model->M[i]) return(model->r[i]*(1.0 + log(M-model->M[i]+1)));

	i = MiLookup(model,M);

	/* Do a linear interpolation. */
	A = (model->M[i+1]-M)/(model->M[i+1]-model->M[i]);
	return (A*model->r[i]+(1.0-A)*model->r[i+1]);
}

/*
** Find the mass enclosed for a given r.
*/
double MLookup(MODEL *model,double r) {
	int i;
	double A;
		
	/* If r is outside of the lookup table, return something. */
	i = model->nTable-1;
    if (r >= model->r[i]) return(model->M[i]*(1.0 + log(r-model->r[i]+1)));

	i = riLookup(model,r);

	/* Do a linear interpolation. */
	A = (model->r[i+1]-r)/(model->r[i+1]-model->r[i]);
	return (A*model->M[i]+(1.0-A)*model->M[i+1]);
}

/*
** Find the density for a given r.
*/
double rhoLookup(MODEL *model,double r) {
	int i;
	double A;
		
	/* If r is outside of the lookup table, return something. */
	i = model->nTable-1;
    if (r >= model->r[i]) return(model->rho[i]*exp(-(r-model->r[i])));

	i = riLookup(model,r);

	/* Do a linear interpolation. */
	A = (model->r[i+1]-r)/(model->r[i+1]-model->r[i]);
	return (A*model->rho[i]+(1.0-A)*model->rho[i+1]);
}

/*
** Find the internal energy for a given r.
*/
double uLookup(MODEL *model,double r) {
	int i;
	double A;
		
	/* If r is outside of the lookup table, return something. */
	i = model->nTable-1;
    if (r >= model->r[i]) return(model->u[i]*exp(-(r-model->r[i])));

	i = riLookup(model,r);

	/* Do a linear interpolation. */
	A = (model->r[i+1]-r)/(model->r[i+1]-model->r[i]);
	return (A*model->u[i]+(1.0-A)*model->u[i+1]);
}
