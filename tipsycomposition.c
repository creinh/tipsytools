/*
 * This program reads a Tipsy binary file and calculates the mass fraction of each material. If a
 * reference density is provided it also computes the mass and composition of the atmosphere.
 *
 * Author:   Christian Reinhardt
 * Date:     05.02.2018
 * Modified: 29.10.2020
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <EOSlib.h>
#include "tipsy.h"

#define INDEX(i, j) (((i)*granite->nTableV) + (j))

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int type, N;
    double dSoft;
    struct base_particle *p;
    struct gas_particle *g;
    double time;
    double *M;
    double *Menv;
    double Mtot, Menvtot;
    double rho0;
    double Z;
    double const MEarth= 5.98e27;
    double const Munit=  9.56072e+25;
    int nMat, iMat, i;

    nMat = EOS_N_MATERIAL_MAX;
    assert(nMat > 0);

    if (argc > 2) {
        fprintf(stderr,"Usage: tipsycomposition <tipsy.std\n");
        fprintf(stderr,"Note: If the user specified a reference density then the mass of the envelope is determined too.\n");
        exit(1);
    }

    // Was a reference density defined?
    if (argc == 2) {
        rho0 = atof(argv[1]);

        // Allocate memory
        Menv = (double *) calloc(nMat, sizeof(double));
    } else {
        rho0 = 0.0;
        Menv = NULL;
    }

    // Allocate memory
    M = (double *) calloc(nMat, sizeof(double));
    printf("nMat=%i\n",nMat);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, "stdin");

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    for (i=0; i<nMat; i++)
    {
        M[i] = 0.0;
        if (Menv != NULL) Menv[i] = 0.0;
    }

    Mtot = 0.0;	
    Menvtot = 0.0;

    // Read particles from the input file
    for (i=0; i<N; i++)
    {
        p = pTipsyRead(in, &type, &dSoft);
        assert(type == TIPSY_TYPE_GAS);
        g = (struct gas_particle *)p;

        iMat = (int) g->metals;

        assert((0<=iMat) && (iMat<nMat));

        M[iMat] += g->mass;
        Mtot += g->mass;

        // Ideal gas is always part of the envelope
        if ((Menv != NULL) && ((g->rho <= rho0) || (iMat == MAT_IDEALGAS)))
        {
            Menv[iMat] += g->mass;
            Menvtot += g->mass;
        }
    }

    printf("Final body: (tipsycomposition)\n");
    for (i=0; i<nMat; i++)
    {
        if (M[i] > 0.0)
            printf("iMat %2i: M=%15.8g = %8.4f ME ( %5.2f %% )\n",
                    i, M[i], M[i]*Munit/MEarth, M[i]/Mtot*100.0);
    }
    printf("Mtot=%15.8g = %5.2f ME\n",Mtot,Mtot*Munit/MEarth);

    if (Menv != NULL)
    {
        printf("\n");
        printf("Envelope (rho<=%.8g):\n",rho0);

        Z = 0.0;

        for (i=0; i<nMat; i++)
        {
            if (Menv[i] > 0.0)
            {
                printf("iMat %3i: M=%15.8g = %8.4f ME ( %5.2f %% )\n",
                        i, Menv[i], Menv[i]*Munit/MEarth, Menv[i]/Menvtot*100.0);
            }

            // Calculate heavy element fraction in the atmosphere
            if (i > MAT_IDEALGAS)
                Z += Menv[i];
        }

        Z /= Menvtot;

        printf("Mtot=%15.8g = %5.2f ME Z= %5.4f\n", Menvtot, Menvtot*Munit/MEarth, Z);
    }

    TipsyFinish(in);

    return 0;
}
