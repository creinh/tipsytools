/*
 * This program reads an equilibrium model from a Tipsy binary file and increases
 * its spin.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		double omega, T;
		int i;

		if (argc != 2) {
			fprintf(stderr,"Usage: tipsyspintupmodel <T> <tipsy.std\n");
			fprintf(stderr,"Note: T has to be provided in code units. \n");
			exit(1);
		}
		
		T = atof(argv[1]);
        /* Allow negative rotation periods corresponding to counter-rotating bodies.*/
//		assert(T > 0.0);

		omega = 2.0*M_PI/T;
	
		fprintf(stderr,"T=%g omega=%g\n", T, omega);

		// Initialize tipsy library
		TipsyInitialize(&in, 0, "stdin");
		TipsyInitialize(&out, 0, NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
		// Read particles from the input file
        for (i=0;i<N;i++)
        {
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
		
			/*
             * We require:
             * v = omega x r
             *
             * So:
             * vx = -y*omega_z + z*omega_y
             * vy = -z*omega_x + x*omega_z
             * vz = -x*omega_y + y*omega_x
             */
			g->vel[0] = -omega*g->pos[1];
			g->vel[1] = omega*g->pos[0];
			g->vel[2] = 0.0;
#if 0
			/* For a rotation around the y axis. */	
			g->vel[0] = -omega*g->pos[2];
			g->vel[1] = 0.0;;
			g->vel[2] = omega*g->pos[0];
#endif
#if 0			
			/* For a rotation around the x axis. */	
			g->vel[0] = 0.0;
			g->vel[1] = -omega*g->pos[2];
			g->vel[2] = omega*g->pos[1];
#endif
			TipsyAddGas(out,g);
		}

		// Write to stdout
		TipsyWriteAll(out,time,NULL);
	
		TipsyFinish(in);
		TipsyFinish(out);
}
