/*
** This program reads a Tipsy binary file and calculates the RMS
** velocity
** 
** v_rms = sqrt(sum(v_i^2)/N)
**
** density
** 
** rho_rms = sqrt(sum(rho_i-rho(r_i)^2)/N)
**
** and internal energy
**
** u_rms = sqrt(sum(u_i-u(r_i)^2)/N)
**
** of the model.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"
#include "model.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		// Model
		MODEL *model;
		double time, R, rho, v_rms, rho_rms, u, u_rms;
		int i, j;

		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_rhochi2 <tipsy.model> <tipsy.std\n");
			exit(1);
		}

		// Read model
		model = modelInit();
		modelRead(model,argv[1]);

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
		v_rms = 0.0;
		rho_rms = 0.0;
		u_rms = 0.0;

		// Read particles from the input file
        for (i = 0; i < N; i++)
        {
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
			
			R = 0.0;

			for (j=0; j<3; j++)
			{
				R += g->pos[j]*g->pos[j];
				v_rms += g->vel[j]*g->vel[j];
			}	
			
			R = sqrt(R);

			// Expected rho and u from the model
			rho = rhoLookup(model,R);
			u = uLookup(model,R);

//			printf("rho_i=%g R=%g rho(R)=%g\n",g->rho,R,rho);
			rho_rms += (g->rho-rho)*(g->rho-rho);
			u_rms += (g->temp-u)*(g->temp-u);
		}
		
		v_rms = sqrt(v_rms/N);
		rho_rms = sqrt(rho_rms/N);
		u_rms = sqrt(u_rms/N);

		printf("%15.7E%15.7E%15.7E%15.7E\n",time, v_rms, rho_rms,u_rms);
		TipsyFinish(in);
}
