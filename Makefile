#
# EOS library
#
TILL_OBJ = ../tillotson/tillotson.o ../tillotson/tillinitlookup.o ../tillotson/tillsplint.o ../tillotson/interpol/brent.o
ANEOS_OBJ = ../ANEOSmaterial/ANEOSmaterial.o ../ANEOSmaterial/interpBilinear.o
REOS3_OBJ = ../reos3/reos3.o
SCVHEOS_OBJ = ../scvh/scvheos.o
EOSLIB_OBJ = ../EOSlib/EOSlib.o ../EOSlib/igeos.o

EOS_OBJ = $(EOSLIB_OBJ)
INCLUDES = -I../EOSlib

ifeq ($(shell test -e ../tillotson/tillotson.h && echo -n yes),yes)
    INCLUDES += -DHAVE_TILLOTSON_H -I../tillotson
    EOS_OBJ += $(TILL_OBJ)
endif

ifeq ($(shell test -e ../ANEOSmaterial/ANEOSmaterial.h && echo -n yes),yes)
    INCLUDES += -DHAVE_ANEOSMATERIAL_H -I../ANEOSmaterial
    EOS_OBJ += $(ANEOS_OBJ)
endif

ifeq ($(shell test -e ../reos3/reos3.h && echo -n yes),yes)
    INCLUDES += -DHAVE_REOS3_H -I../reos3
    EOS_OBJ += $(REOS3_OBJ)
endif

ifeq ($(shell test -e ../scvh/scvheos.h && echo -n yes),yes)
    INCLUDES += -DHAVE_SCVHEOS_H -I../scvh
    EOS_OBJ += $(SCVHEOS_OBJ)
endif

# Tipsy API
TIPSY_API_OBJ = tipsy.o

MODEL_OBJ = model.o

EXE = tipsyarray tipsycutarray tipsyprofile tipsytoascii tipsycomposition tipsyspinupmodel tipsy_vrms tipsy_rms tipsy_chi2 tipsy_mark_mat_origin tipsy_imprint_u tipsyradius tipsycutmark tipsycutmarkinv tipsy_overwrite_hsmooth tipsy_rotate_y_90

#CODE_DEF = -DTILL_PRESS_NP -DTILL_OUTPUT_ALL_WARNINGS -DTILL_PRESS_MELOSH
CODE_DEF = -DTILL_PRESS_NP -DTILL_OUTPUT_ALL_WARNINGS -DTILL_VERBOSE

# GNU Science library (uncomment if not needed)
GSL_LIB = -lgsl -lgslcblas

# tirpc library (needed if glibc >= 2.32)
RPC_LIB = -ltirpc

CFLAGS ?= -O3 $(CODE_DEF) $(INCLUDES) -Wall -std=gnu99

LIBS ?= -lm $(GSL_LIB) $(RPC_LIB)

default:
	@echo "Please specify which tool you want to make."

all:
	default

# Read a quantity (e.g., density, temperature, material) from a tipsy binary file and print it to a tipsy array file that can be read with tipsy
tipsyarray: tipsyarray.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc $(CFLAGS) -o tipsyarray tipsyarray.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Read a Tipsy array and output all particles that have a given value to a Tipsy binary file
tipsycutarray: tipsycutarray.o $(TIPSY_API_OBJ)
	cc -o tipsycutarray tipsycutarray.o $(TIPSY_API_OBJ) $(LIBS)

# Read the particle data to an ASCII file and print additional information like pressure and temperature
tipsyprofile: CFLAGS += -DSCVH_NOEXIT
tipsyprofile: tipsyprofile.o $(TIPSY_API_OBJ) $(EOS_OBJ) 
	cc $(CFLAGS) -o tipsyprofile tipsyprofile.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Read the particle data to an ASCII file and print additional information like pressure and temperature
tipsyascii: tipsyascii.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsyascii tipsyascii.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Fix IC from ballic so that all particles are on the same isentrope
tipsy_fix_model: tipsy_fix_model.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	$(CC) $(CFLAGS) -o tipsy_fix_model tipsy_fix_model.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Mark the particles according to their material and origin (target or impactor)
tipsy_mark_mat_origin: tipsy_mark_mat_origin.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_mark_mat_origin tipsy_mark_mat_origin.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Convert from Tipsy binary to ASCII
tipsytoascii: tipsytoascii.o $(TIPSY_API_OBJ)
	cc -o tipsytoascii tipsytoascii.o $(TIPSY_API_OBJ) $(LIBS)

# Convert from ASCII to Tipsy binary
asciitotipsy: asciitotipsy.o $(TIPSY_API_OBJ)
	cc -o asciitotipsy asciitotipsy.o $(TIPSY_API_OBJ) $(LIBS)

# Calculates the mass fraction of the different materials
tipsycomposition: tipsycomposition.o $(TIPSY_API_OBJ)
	cc -o tipsycomposition tipsycomposition.o $(TIPSY_API_OBJ) $(LIBS)

# Convert from Tipsy binary to ASCII
tipsyspinupmodel: tipsyspinupmodel.o $(TIPSY_API_OBJ)
	cc -o tipsyspinupmodel tipsyspinupmodel.o $(TIPSY_API_OBJ) $(LIBS)

# Calculte RMS velocity of a model
tipsy_vrms: tipsy_vrms.o $(TIPSY_API_OBJ)
	cc -o tipsy_vrms tipsy_vrms.o $(TIPSY_API_OBJ) $(LIBS)

# Calculte RMS velocity, density and internal energy energy of a model
tipsy_rms: tipsy_rms.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_rms tipsy_rms.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Calculte Chi2 of the density and internal energy of a model
tipsy_chi2: tipsy_chi2.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_chi2 tipsy_chi2.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Set the internal energy of all particles to match a profile
tipsy_imprint_u: tipsy_imprint_u.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_imprint_u tipsy_imprint_u.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Set the hsmooth value read from a file
tipsy_overwrite_hsmooth: tipsy_overwrite_hsmooth.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_overwrite_hsmooth tipsy_overwrite_hsmooth.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Adds an angular velocity to the model and rotates it using euler angles
tipsy_add_angular_velocity_and_rotate: tipsy_add_angular_velocity_and_rotate.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_add_angular_velocity_and_rotate tipsy_add_angular_velocity_and_rotate.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Rotate the model by 90 degrees around the y axis
tipsy_rotate_y_90: tipsy_rotate_y_90.o $(TIPSY_API_OBJ) $(MODEL_OBJ)
	cc -o tipsy_rotate_y_90 tipsy_rotate_y_90.o $(TIPSY_API_OBJ) $(MODEL_OBJ) $(LIBS)

# Calculate the radius of a model (in two different ways)
tipsyradius: tipsyradius.o $(TIPSY_API_OBJ)
	cc -o tipsyradius tipsyradius.o $(TIPSY_API_OBJ) $(LIBS)

#
# Output one particle's data.
#
tipsy_output_particle: tipsy_output_particle.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_output_particle tipsy_output_particle.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Insert the density from an array file into a tipsy binary file
tipsy_insert_den: tipsy_insert_den.o $(TIPSY_API_OBJ) 
	cc -o tipsy_insert_den tipsy_insert_den.o $(TIPSY_API_OBJ) $(LIBS)

# Insert the density from an array file into a tipsy binary file
tipsy_checkic: tipsy_checkic.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_checkic tipsy_checkic.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# Insert the density from an array file into a tipsy binary file
tipsy_etot: tipsy_etot.o $(TIPSY_API_OBJ) $(TILL_OBJ)
	cc -o tipsy_etot tipsy_etot.o $(TIPSY_API_OBJ) $(TILL_OBJ) $(LIBS)

# Split the tipsyfile in the xy plane into 4 quadrants
tipsy_split_xy: tipsy_split_xy.o $(TIPSY_API_OBJ)
	cc -o tipsy_split_xy tipsy_split_xy.o $(TIPSY_API_OBJ) $(LIBS)

#
# Calculate the pressure for each particle.
#
tipsy_pressure: tipsy_pressure.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_pressure tipsy_pressure.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

# 
# Extract marked particles from a tipsy binary file.
#
tipsycutmark: tipsycutmark.o $(TIPSY_API_OBJ)
	cc -o tipsycutmark tipsycutmark.o $(TIPSY_API_OBJ) $(LIBS)

# 
# Remove marked particles from a tipsy binary file.
#
tipsycutmarkinv: tipsycutmarkinv.o $(TIPSY_API_OBJ)
	cc -o tipsycutmarkinv tipsycutmarkinv.o $(TIPSY_API_OBJ) $(LIBS)

# 
# Remove marked particles from a tipsy binary file.
#
tipsy_jtot: tipsy_jtot.o $(TIPSY_API_OBJ)
	cc -o tipsy_jtot tipsy_jtot.o $(TIPSY_API_OBJ) $(LIBS)

# 
# Calculate the COM of a tipsy binary file.
#
tipsy_com: tipsy_com.o $(TIPSY_API_OBJ)
	cc -o tipsy_com tipsy_com.o $(TIPSY_API_OBJ) $(LIBS)

# 
# Move the COM of a tipsy binary file.
#
tipsy_move_com: tipsy_move_com.o $(TIPSY_API_OBJ)
	cc -o tipsy_move_com tipsy_move_com.o $(TIPSY_API_OBJ) $(LIBS)

#
# Print data of one particle.
#
tipsy_particleinfo: tipsy_particleinfo.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_particleinfo tipsy_particleinfo.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

#
# Calculate the total internal energy.
#
tipsy_int_energy: tipsy_int_energy.o $(TIPSY_API_OBJ) $(EOS_OBJ)
	cc -o tipsy_int_energy tipsy_int_energy.o $(TIPSY_API_OBJ) $(EOS_OBJ) $(LIBS)

#
# Mark particles from an array file.
#
tipsy_markarray: tipsy_markarray.o $(TIPSY_API_OBJ)
	cc -o tipsy_markarray tipsy_markarray.o $(TIPSY_API_OBJ) $(LIBS)

#
# Calculate the angular momentum.
#
tipsy_calc_angular_momentum: tipsy_calc_angular_momentum.o $(TIPSY_API_OBJ)
	cc -o tipsy_calc_angular_momentum tipsy_calc_angular_momentum.o $(TIPSY_API_OBJ) $(LIBS)

#
# Calculate the moment of inertia for a given enclosed mass.
#
tipsy_moi_vs_mass: tipsy_moi_vs_mass.o $(TIPSY_API_OBJ)
	cc -o tipsy_moi_vs_mass tipsy_moi_vs_mass.o $(TIPSY_API_OBJ) $(LIBS)

#
# Calculate the mass fraction.
#
tipsy_mass_frac: tipsy_mass_frac.o $(TIPSY_API_OBJ)
	cc -o tipsy_mass_frac tipsy_mass_frac.o $(TIPSY_API_OBJ) $(LIBS)

#
# Update material ids of the Tillotson EOS library to version 3.
#
tipsy_update_mat_v3: tipsy_update_mat_v3.o $(TIPSY_API_OBJ) $(TILL_OBJ)
	cc -o tipsy_update_mat_v3 tipsy_update_mat_v3.o $(TIPSY_API_OBJ) $(TILL_OBJ) $(LIBS)

#
# Print the phase of each particle to an array file.
#
tipsy_till_phase_array: tipsy_till_phase_array.o $(TIPSY_API_OBJ) $(TILL_OBJ)
	cc -o tipsy_till_phase_array tipsy_till_phase_array.o $(TIPSY_API_OBJ) $(TILL_OBJ) $(LIBS)

#
# Move the particles to the COM reference frane.
#
tipsy_transform_com: tipsy_transform_com.o $(TIPSY_API_OBJ)
	cc -o tipsy_transform_com tipsy_transform_com.o $(TIPSY_API_OBJ) $(LIBS)

#
# Convert a 1D model from Helled & Bodenheimer (2010) to a ballic model.
#
model_ravit: model_ravit.o $(EOS_OBJ)
	cc -o model_ravit model_ravit.o $(EOS_OBJ) $(LIBS)

clean:
	rm $(EOS_OBJ) *.o

cleanall:
	rm $(EXE) $(EOS_OBJ) *.o
