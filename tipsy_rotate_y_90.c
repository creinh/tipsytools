/*
** This program rotates a model around the y axis by 90 degrees
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

int main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		int i;

		if (argc != 1) {
			fprintf(stderr,"Usage: tipsy_rotate_y_90 <tipsy.std\n");
			exit(1);
		}


		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		// Read particles from the input file
	        for (i = 0; i < N; i++)
        	{
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
			double x = g->pos[0];
			double z = g->pos[2];
			double vx = g->vel[0];
			double vz = g->vel[2];
			g->pos[0] = z;
			g->vel[0] = vz;
			g->pos[2] = -x;
			g->vel[2] = -vx;
			TipsyAddGas(out,g);
		}

		// Write to stdout
 		TipsyWriteAll(out,time,NULL);

		fprintf(stderr,"t=%g\n",time);
		TipsyFinish(in);
		TipsyFinish(out);
		return 0;
}
