/*
 * This program reads a Tipsy binary file and calculates the total specific
 * angular momentum of all particles.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

void TipsyCalcCOM(TCTX in, double *pcom, double *pvcom, double *pMass)
{
    struct base_particle *p;
	struct gas_particle *g;
	double dSoft,ipMass;
	int type,N,i,j;

	N = iTipsyNumParticles(in);

	// Calculate COM
	*pMass = 0.0;

	for (j=0; j<3; j++)
	{
	    pcom[j] = 0.0;
		pvcom[j] = 0.0;
	}
        
    for (i=0; i<N; i++)
    {
        for (j = 0; j < 3; j++)
		{
			pcom[j] += in->gp[i].mass*in->gp[i].pos[j];
			pvcom[j] += in->gp[i].mass*in->gp[i].vel[j];
		}
		*pMass += in->gp[i].mass;
	}
		
	ipMass = *pMass;
		
    for (j = 0; j < 3; j++)
	{
		pcom[j] *= 1.0/ipMass;
		pvcom[j] *= 1.0/ipMass;
	}
}

void main(int argc, char **argv) {
    // Tipsy library
	TCTX in;
	int type, N;
	double dSoft;
	struct gas_particle *g;
	double time;
    double tmp;
    double mass;
    double j[3];
    double L[3];
    double r_com[3];
    double v_com[3];
	int i, k;

	if (argc != 2) {
		fprintf(stderr,"Usage: tipsy_jtot <tipsy.std>\n");
        exit(1);
	}

	// Initialize tipsy library
	TipsyInitialize(&in, 0, argv[1]);

	N = iTipsyNumParticles(in);
	time = dTipsyTime(in);

    TipsyReadAll(in);

    // Determine the COM position and velocity
    TipsyCalcCOM(in, r_com, v_com, &mass);

    printf("COM:\n");
      
    printf("r=");
    for (i=0; i<3; i++)
    {
        printf(" %15.7E", r_com[i]);
    }
    printf("\n");

    printf("v=");
    for (i=0; i<3; i++)
    {
        printf(" %15.7E", v_com[i]);
    }
    printf("\n");
    printf("M= %15.7E\n", mass);
    printf("\n");

    // Set all vectors to zero
    for (i=0; i<3; i++)
    {
        j[i] = 0.0;
        L[i] = 0.0;
    }

    // Calculate the angular momentum for all particles
    for (i = 0; i<N; i++)
    {
        // Substract the com
        for (k=0; k<3; k++)
        {
            in->gp[i].pos[k] -= r_com[k];
            in->gp[i].vel[k] -= v_com[k];
        }
        // L = r x p
        // Lx = m*(y*vz - z*vy)
        L[0] += in->gp[i].mass*(in->gp[i].pos[1]*in->gp[i].vel[2] - in->gp[i].pos[2]*in->gp[i].vel[1]);
        // Ly = m*(z*vx - x*vz)
        L[1] += in->gp[i].mass*(in->gp[i].pos[2]*in->gp[i].vel[0] - in->gp[i].pos[0]*in->gp[i].vel[2]);
        // Lz = m*(x*vy - y*vx)
        L[2] += in->gp[i].mass*(in->gp[i].pos[0]*in->gp[i].vel[1] - in->gp[i].pos[1]*in->gp[i].vel[0]);
    }
		
    tmp = 0.0;

    // Calculate j (note this is not the same as j=r x v.
    for (i=0; i<3; i++)
    {
        j[i] = L[i]/mass;
    }

    printf("Total specific angular momentum:\n");
    for (i=0; i<3; i++)
    {
        printf("j[%i]= %15.7E ", i, j[i]);
        tmp += j[i]*j[i];
    }
    printf("\n");

//    printf("jtot= %g\n", sqrt(tmp));
    printf("jtot= %g\n", sqrt(j[0]*j[0]+j[1]*j[1]+j[2]*j[2]));
    printf("\n");

    printf("Total angular momentum:\n");
    for (i=0; i<3; i++)
    {
        printf("L[%i]= %15.7E ", i, L[i]);
//      tmp += j[i]*j[i];
    }
    printf("\n");

    printf("Ltot= %g\n", sqrt(L[0]*L[0]+L[1]*L[1]+L[2]*L[2]));
    printf("\n");
		
    TipsyFinish(in);
}
