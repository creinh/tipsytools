/*
 * Author:   Christian Reinhardt
 * Created:  31.01.2018
 * Modified: 04.01.2022
 *
 * This program reads a Tipsy binary file and prints the particle data (e.g. pos, vel, mass,
 * pressure, temp) to an ASCII file.
 *
 * NOTE: Since the density is calculated during each SPH step a .den output file from Gasoline can
 * be provided as an optional command line argument to set the particle's density.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <argp.h>
#include <EOSlib.h>
#include "tipsy.h"

/*
 * Parse command line arguments with GNU argp.
 */
const char *argp_program_version = "tipsyprofile v1.0";
const char *argp_program_bug_address = "christian.reinhardt@ics.uzh.ch";

/* Description of the program. */
static char doc[] = "This program reads a Tipsy binary file and prints the particle data (e.g. pos, vel, mass, pressure, temp) to an ASCII file.";


/* Description of input arguments. */
static char args_doc[] = "[Binary]";

/* Define number of required arguments. */
#define NUM_ARGS_MAX 100

/* Start from 256 because we do not want short options. */
enum OPTIONS {
    OPT_SET_KPCUNIT = 256,
    OPT_SET_MSOLUNIT,
    OPT_SET_RHOSURF,
    OPT_SET_GAMMA,
    OPT_SET_MEANMOLMASS,
    OPT_SET_TEMP,
    OPT_SET_DEN
};

/* Optional arguments. */
static struct argp_option options[] = {
    { "dKpcUnit", OPT_SET_KPCUNIT, "KPCUNIT", 0, "Set length unit in kpc (default: 2.06701e-13)." },
    { "dMsolUnit", OPT_SET_MSOLUNIT, "MSOLUNIT", 0, "Set mass unit in Solar masses (default: 4.80438e-08)." },
    { "dConstGamma", OPT_SET_GAMMA, "GAMMA", 0, "Set gamma for the ideal gas eos (default: 5/3)." },
    { "dMeanMolMass", OPT_SET_MEANMOLMASS, "MEANMOLMASS", 0, "Set mean molecular mass in units of m_H (default: 1.0)." },
    { "bTemp", OPT_SET_TEMP, 0, 0, "Use temperature as input (as required by pkdgrav3)." },
    { "DenArrayFile", OPT_SET_DEN, "FILENAME", 0, "Tipsy array file containing the particle's densities." },
    { 0 }
};

struct arguments {
    /* Required arguments. */
    char *args[NUM_ARGS_MAX];
    int nArgs;
    /* Options. */
    double dKpcUnit;
    double dMsolUnit;
    bool bKpcUnitSet;
    bool bMsolUnitSet;
    double dConstGamma;
    double dMeanMolMass;
    char *DenArrayFile; 
    bool bTemp;
    bool bDenArray;
};

error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct arguments *arguments = state->input;

    switch (key) {
        case OPT_SET_KPCUNIT:
            arguments->bKpcUnitSet = true;
            arguments->dKpcUnit = atof(arg);
            break;
        case OPT_SET_MSOLUNIT:
            arguments->bMsolUnitSet = true;
            arguments->dMsolUnit = atof(arg);
            break;
        case OPT_SET_GAMMA:
            arguments->dConstGamma = atof(arg);
            break;
        case OPT_SET_MEANMOLMASS:
            arguments->dMeanMolMass = atof(arg);
            break;
        case OPT_SET_TEMP:
            arguments->bTemp = true;
            break;
        case OPT_SET_DEN:
            arguments->DenArrayFile = arg;
            arguments->bDenArray = true;
            break;
        case ARGP_KEY_ARG:
            /* Too many arguments. */
            if (state->arg_num >= arguments->nArgs) {
                argp_usage(state);
            }
            arguments->args[state->arg_num] = arg;    
            break;
        case ARGP_KEY_END:
            if (state->arg_num < arguments->nArgs) {
                /* Not enough arguments. */
                argp_usage(state);
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

void main(int argc, char **argv) {
    // Tipsy library
	TCTX in;
	int type, N;
	double dSoft;
	struct base_particle *p;
	struct gas_particle *g;
    // EOS library
    EOSMATERIAL **Mat;
    double dKpcUnit = 2.06701e-13;
    double dMsolUnit = 4.80438e-08;
    // These parameters are currently only used for the Tillotson EOS
    double rhomin = 1e-4;
    double rhomax = 200.0;
    double vmax = 1200.0;
    int nTableRho = 1000;
    int nTableV = 1000;
    int iMat;
    int nMat = EOS_N_MATERIAL_MAX;
	double R;
    double M;
    double rho;
    struct arguments arguments;
    char *DenArrayFile;
    bool bTemp;
    bool bDenArray;
    struct igeosParam igParam;
    int iRet;
    FILE *fp;

    /* Set number of required arguments. */
    arguments.nArgs = 1;

    /* Initialize default arguments. */
    arguments.dKpcUnit = 2.06701e-13;
    arguments.dMsolUnit = 4.80438e-08;
    arguments.bKpcUnitSet = false;
    arguments.bMsolUnitSet = false;
    arguments.dConstGamma = 5.0/3.0;
    arguments.dMeanMolMass = 1.0;
    arguments.bTemp = false;
    arguments.bDenArray = false;
    
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    if (arguments.bKpcUnitSet != arguments.bMsolUnitSet) {
        fprintf(stderr, "Error: only one unit (dKpcUnit or dMsolUnit) was specified by the user.\n");
        exit(1);
    }

    dKpcUnit = arguments.dKpcUnit;
    dMsolUnit = arguments.dMsolUnit;
    
    /* Parameters for ideal gas. */    
    igParam.dConstGamma = arguments.dConstGamma;
    igParam.dMeanMolMass = arguments.dMeanMolMass;

    bTemp = arguments.bTemp;
    bDenArray = arguments.bDenArray;

    if (bDenArray) DenArrayFile = arguments.DenArrayFile;
	
    // Print version information
    fprintf(stderr,"EOS library version: %s\n", EOS_VERSION_TEXT);
	
    // Allocate memory for the array that containts all materials in the EOS library
    Mat = (EOSMATERIAL**) calloc(nMat, sizeof(EOSMATERIAL *));
    fprintf(stderr, "Memory for the EOS library is allocated (nMat= %i).\n", nMat);

	// Initialize the tipsy library.
	TipsyInitialize(&in, 0, arguments.args[0]);

	N = iTipsyNumParticles(in);
 
    // Open the density array file if needed.
    if (bDenArray) {
        int nArray;

        fprintf(stderr, "Tipsy array file: %s\n", DenArrayFile);
        
        fp = fopen(DenArrayFile, "r");
        assert(fp != NULL);

        /*
         * Read the array file header and assert that the number of particles
         * is consistent.
         */
        iRet = fscanf(fp, "%d", &nArray);

        assert(nArray == N);
    }

	// Print header
    printf("#%14s%15s%15s%15s%4s%15s%15s\n", "R", "rho", "M", "u", "mat", "P", "T");

	M = 0.0;

    /*
     * Read all particle from the binary file and print the desired quantities.
     */
    for (int i=0; i<N; i++)
    {
        p = pTipsyRead(in, &type, &dSoft);
		assert(type == TIPSY_TYPE_GAS);
		g = (struct gas_particle *)p;
			
		iMat = (int) g->metals;

		R = sqrt(g->pos[0]*g->pos[0]+g->pos[1]*g->pos[1]+g->pos[2]*g->pos[2]);
        // This is BS, we have to order to particles according to radius if we do that!
		//M += g->mass;
		M = g->mass;

        // Read the density either from tipsy.den or directly from the binary file.
        if (bDenArray) {
            iRet = fscanf(fp, "%lf", &rho);
            assert(iRet > 0);
            assert(rho >= 0.0);
        } else {
            rho = g->rho;
        }

        // Initialize the material and look up table if needed.
        if (Mat[iMat] == NULL) {
            fprintf(stderr, "iMat %i not yet initialized.\n", iMat);

            if (iMat == EOSIDEALGAS) {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, &igParam);
            } else {
                Mat[iMat] = EOSinitMaterial(iMat, dKpcUnit, dMsolUnit, NULL);
            }
            assert(Mat[iMat] != NULL);
            fprintf(stderr, "Done.\n");
            fprintf(stderr, "\n");
        }

        if (Mat[iMat]->bEntropyTableInit == EOS_FALSE) {
            if (iMat != MAT_IDEALGAS)
            {
                fprintf(stderr, "iMat %i entropy lookup table not yet initialized.\n", iMat);
                EOSinitIsentropicLookup(Mat[iMat], NULL);
                fprintf(stderr, "\n");
            }
        } 

        if (bTemp) {
            printf("%15.7E%15.7E%15.7E%15.7E%4i%15.7E%15.7E\n", R, rho, M, EOSUofRhoT(Mat[iMat],rho,g->temp), iMat, EOSPofRhoT(Mat[iMat], rho, g->temp), g->temp);
        } else {
            printf("%15.7E%15.7E%15.7E%15.7E%4i%15.7E%15.7E\n", R, rho, M, g->temp, iMat, EOSPofRhoU(Mat[iMat], rho, g->temp), EOSTofRhoU(Mat[iMat], rho, g->temp));
        }
	}
	
	// Free memory
	for (int i=0; i<nMat; i++)
	{
        if (Mat[i] != NULL) EOSfinalizeMaterial(Mat[i]);
	}
    
    if (bDenArray) fclose(fp);

    TipsyFinish(in);
}
