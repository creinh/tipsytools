/*
 ** This program stores the density from an tipsy density array in a tipsy binary file.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

#define LINE_SIZE 128
void main(int argc, char **argv) {
		// Tipsy library
		TCTX in,out;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		FILE *fp = NULL;
		int iRet, N_array;
		double rho;
        int i;

		// Check command line arguments
		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_insert_den <input.den> <input.std\n");
			exit(1);
		}

		// Open text file
		fp = fopen(argv[1], "r");
		assert(fp != NULL);
		
		// Read the array file header
		iRet = fscanf(fp, "%d", &N_array);
	
		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		TipsyInitialize(&out,0,NULL);

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);

		// Make sure that both files have the same number of particles
		assert(N_array == N);

		// Read all particles from the input file
        for (i = 0; i < N; i++)
        {
                p = pTipsyRead(in,&type,&dSoft);
                assert(type == TIPSY_TYPE_GAS);
/*
                if (type == TIPSY_TYPE_GAS)
				{
				} else {
					fprintf(stderr,"Particle %i is not a gas particle\n",i);
				}
*/
				// Read the particle's density and store it in g->den
				iRet = fscanf(fp, "%lf", &rho);
//				printf("%.8g\n", rho);
				assert(iRet > 0);
				assert(rho >= 0.0);

				g = (struct gas_particle *)p;
				g->rho = rho;

				// Add to output file
				TipsyAddGas(out,g);
		}
		
		// Write to stdout
        TipsyWriteAll(out,time,NULL);
		
		fclose(fp);
		TipsyFinish(in);
		TipsyFinish(out);
}



