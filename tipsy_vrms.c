/*
** This program reads a Tipsy binary file and calculates the RMS velocity
** 
** v_rms = sqrt(sum(v_i^2)/N)
**
** of the model.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

int main(int argc, char **argv) {
		// Tipsy library
		TCTX in;
		int type, N;
		double dSoft;
		struct base_particle *p;
		double time,v_rms;
		int i, j;

		if (argc > 1) {
			fprintf(stderr,"Usage: tipsy_vrms <tipsy.std\n");
			exit(1);
		}
			
		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");

		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);
		
		// Print time
		printf("%15.7E",time);

		v_rms = 0.0;

		// Read particles from the input file
        for (i = 0; i < N; i++)
        {
			p = pTipsyRead(in,&type,&dSoft);
			
			// Velocities
			for (j=0; j<3; j++)
			{
				v_rms += p->vel[j]*p->vel[j];
			}	
		}
		
		v_rms = sqrt(v_rms/N);	
		printf("%15.7E\n",v_rms);
		TipsyFinish(in);
        return 0;
}
