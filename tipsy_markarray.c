/*
 * This program is a command line version of the tipsy command markarray.
 *
 * It marks particles who's array values are between dMin and dMax.
 *
 * Author:   Christian Reinhardt
 * Date:     29.05.2018
 * Modified: 
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

#define LINE_SIZE 128

struct tipsyArray {
    int nTot;
    double *dValue;
};

struct tipsyArray *TipsyReadArray(char *chFilename) {
    struct tipsyArray *array;
    FILE *fp;
    int iRet;
    int i;

    // Allocate memory and open the mark file
    array = calloc(1, sizeof(struct tipsyArray));
    
    if (array == NULL)
        return array;

    fp = fopen(chFilename, "r");
    
    if (fp == NULL) {
        free(array);
        return array;
    }

    // Read the header
    iRet = fscanf(fp, "%d", &array->nTot);

    if (iRet <= 0) {
        fclose(fp);
        free(array);
        return array;
    }

    // Allocate memory for the array data
    array->dValue = calloc(array->nTot, sizeof(double));

    if (array->dValue == NULL) {
        fclose(fp);
        free(array);
        return array;
    }

    for (i=0; i<array->nTot; i++) {
        iRet = fscanf(fp, "%lf", &array->dValue[i]);
    }

    fclose(fp);

    return array;
}



int main(int argc, char **argv) {
    struct tipsyArray *array;
    double dMin;
    double dMax;
    int i;

    if (argc != 4) {
        fprintf(stderr,"Usage: tipsy_markarray <tipsy.array> <dMin> <dMax> \n");
        exit(1);
    }

    dMin = atof(argv[2]);
    dMax = atof(argv[3]);

    assert(dMin <= dMax);

    // Read array
    array = TipsyReadArray(argv[1]);
    assert(array != NULL);
    assert(array->nTot > 0);

    // Print header for the mark file
    printf("%d %d %d\n", array->nTot, array->nTot, 0);

    // Read particles from the array file file
    for (i=0; i<array->nTot; i++)
    {
        if ((array->dValue[i] >= dMin) && (array->dValue[i] <= dMax)) {
            // Note that in mark files the particle id starts with 1
            printf("%d\n", i+1);
        }
    }

    return 0;
}
