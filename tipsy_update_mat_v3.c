/*
 * Change the material version from Tillotson EOS library v1.0 to v3.0.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tillotson/tillotson.h"
#include "tillotson/tilloldmatid.h"
#include "tipsy.h"

/*
 * Convert the material ids from v1 to v3.
 */
int iConvertMatv3(int iMat) {
    switch(iMat)
    {
      case GRANITE_V1:
            /*
             * Granite: Benz et al. (1986).
             */
            iMat = GRANITE;
            break;
        case IRON_V1:
            /*
             * Iron: Benz et al. (1987).
             */
            iMat = IRON;
            break;
        case BASALT_V1:
            /*
             * Basalt: Benz & Asphaug (1999).
             */
            iMat = BASALT;
            break;
        case ICE_V1:
            /*
             * Ice: Benz & Asphaug (1999).
             */
            iMat = ICE;
            break;
        case WATER_V1:
            /*
             * Water: O'Keefe & Ahrens (1982), 5 in the Tillotson library.
             */
            iMat = WATER;
            break;
        default:
            /* Unknown material */
            assert(0);
    }

    return(iMat);
} 

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in, out;
    TILLMATERIAL **tillMat;
    int nMat;
    int iMat;
    double time;
    int N;
    int i;

    // Check command line arguments
    if (argc != 3) {
        fprintf(stderr,"Usage: tipsy_update_mat_v3 <input.std> <output.std>\n");
        exit(1);
    }

    nMat = TILL_N_MATERIAL_MAX;

    fprintf(stderr, "Tillotson EOS library version: %s\n", TILL_VERSION_TEXT);

    tillMat = (TILLMATERIAL **) calloc(nMat, sizeof(TILLMATERIAL *));
    assert(tillMat != NULL);


    TipsyInitialize(&in, 0, argv[1]);
    TipsyInitialize(&out, 0, NULL);

    // Read all particles
    TipsyReadAll(in);
    
    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    for (i=0; i<N; i++) {
        // Convert the material id
        iMat = iConvertMatv3((int) in->gp[i].metals);
        
        //fprintf(stdout, "Old: %i new: %i\n", iMat, (int) in->gp[i].metals);

        if (tillMat[iMat] == NULL) {
            // We do not convert the material constants to code units
            fprintf(stderr, "Init material %i for particle %i\n", iMat, i);
            tillMat[iMat] = tillInitMaterial(iMat, 0.0, 0.0);
            fprintf(stderr, "Done.\n");
            assert(tillMat[iMat] != NULL);
        }
        
        in->gp[i].metals = iMat;

        TipsyAddGas(out, &in->gp[i]);
    }

    // Print all materials
    fprintf(stderr, "Materials:\n");
    for (i=0; i<nMat; i++) {
        if (tillMat[i] != NULL) {
            tillPrintMat(tillMat[i]);
        }
    }
	// Write to stdout
    TipsyWriteAll(out, time, argv[2]);

    TipsyFinish(in);
    TipsyFinish(out);

    return 0;
}

