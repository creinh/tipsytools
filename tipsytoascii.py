"""
This script reads IC from and tipsy binary and an ASCII file and compares the results.
"""
from matplotlib import *
from numpy import *
from matplotlib.pyplot import *
from sys import *

# Used to read tipsy binary files
import pynbody

# For the colormaps
from brewer2mpl import *

# Set a font
rcParams['font.family'] = 'serif'
#rcParams['font.size'] = 14.0

if len(argv) != 3:
		print "Usage: profileden.py tipsyfile.std tipsyfile.txt"
		exit(1)

file1 = argv[1]
file2 = argv[2]

"""
We do not convert code units to cgs here because L=1RE is a good
unit to visualize a planet. But we want to convert the density
to cgs.
"""

bin = pynbody.load(file1)

pos = bin.gas["pos"]
x = pos[:,0]
y = pos[:,1]
z = pos[:,2]
den = bin.gas["rho"]
temp = bin.gas["temp"]

# Load ASCII file
data = loadtxt(file2, skiprows=2)

"""
# Unit convertion factors
ErgPerGmUnit = 9998228982.69		# erg/g
GmPerCcUnit = 0.368477421278		# g/cc
SecUnit = 6378.6921465				# s
Lunit = 637812728.278				# cm
Munit = 9.5607162e+25				# g

# Values for granite
us = 3.5
us2 = 18.0
rho0 = 7.33

# Convert to cgs
temp *= ErgPerGmUnit
#rho0 *= GmPerCcUnit

#figure(figsize=(13,10))
"""

# Make a slice trough the model
i = where(abs(z) < 0.1)

x_slice = x[i]
y_slice = y[i]
z_slice = z[i]

x2 = data[:,3]
y2 = data[:,4]
z2 = data[:,5]

print max(abs(x-x2))
print max(abs(y-y2))
print max(abs(z-z2))

#scatter(x_slice,y_slice,s=4,c=temp[i],cmap='rainbow',linewidth=0.01)
scatter(x_slice,y_slice,s=4,c='red',linewidth=0.01)
scatter(x2[i],y2[i],s=4,c='blue',linewidth=0.01)
#colorbar()

# Set the size of the plot
xlim(-1.1,1.1)
ylim(-1.1,1.1)

xlabel('Radius [$R_{\oplus}$]')
ylabel('Radius [$R_{\oplus}$]')

show()

# We need 300 dpi for the small format and 150 for the large one
savefig('tipsytoascii.png', dpi=150, bbox_inches='tight')
close()

print "Done."
