/*
 * Calculate the angular momentum of each particle in a tipsy binary file.
 *
 * Author: Christian Reinhardt
 * Date:   27.01.2019
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "tipsy.h"

//#define DOUBLE_POS

/*
 * We allocate an array as described in the Numerical Recipes.
 */	
double **dMatrixAlloc(int nRow, int nCol)
{
    double **array;
	int i;

	array = (double**) calloc(nRow, sizeof(double*));
	array[0] = (double*) calloc(nRow*nCol, sizeof(double));

	assert(array != NULL);

	/* Set a pointer to each row. */
	for (i=1; i<nRow; i++)
	{
		array[i] = array[i-1]+nCol;
	}

	return array;
}

/*
 * Calculate the center of mass (COM) of all particles.
 */
int iTipsyCalcCOM(TCTX in, double *pcom, double *pvcom, double *pMass)
{
    double ipMass;
    int N;
    int iRet;
    int i, j;
    
    iRet = 0;

    N = iTipsyNumParticles(in);

    // Calculate COM
    *pMass = 0.0;

    for (j=0; j<3; j++)
    {
        pcom[j] = 0.0;
        pvcom[j] = 0.0;
    }

    for (i=0; i<N; i++)
    {
        for (j=0; j<3; j++)
        {
            pcom[j] += in->gp[i].mass*in->gp[i].pos[j];
            pvcom[j] += in->gp[i].mass*in->gp[i].vel[j];
        }
        *pMass += in->gp[i].mass;
    }

    ipMass = *pMass;
    
    if (ipMass < 0.0)
        iRet = 1;

    for (j=0; j<3; j++)
    {
        pcom[j] *= 1.0/ipMass;
        pvcom[j] *= 1.0/ipMass;
    }

    return iRet;
}

#if 0
/*
 * Calculate the angular momentum vector of all particles.
 */
int iTipsyCalcAngularMomentum(TCTX *pin, double **L, double *pcom, double *pvcom, double *pMass)
{



}
#endif

int main(int argc, char **argv) {
    // Tipsy library
    TCTX in;
    int N;
    double time, mass;
    double *com,*vcom;
    double L[3];
    double Lx, Ly, Lz;
    double Ltot;
    int i,j;

    // Check command line arguments
    if (argc != 2) {
        fprintf(stderr,"Usage: tipsy_calc_angular_momentum <input.std>\n");
        exit(1);
    }

    // Allocate memory	
    com = calloc(3, sizeof(double));
    assert(com != NULL);

    vcom = calloc(3, sizeof(double));
    assert(vcom != NULL);

    // Initialize tipsy library
    TipsyInitialize(&in, 0, argv[1]);
    TipsyReadAll(in);

    fprintf(stderr, "Calculating COM...\n");

    // Calculate COM
    if (iTipsyCalcCOM(in, com, vcom, &mass) != 0)
    {
        fprintf(stderr, "Calculating COM failed.\n");
        return 1;
    }

    N = iTipsyNumParticles(in);
    time = dTipsyTime(in);

    // Allocate the array
    // L = dMatrixAlloc(N, 3);

    fprintf(stderr,"COM: ");
    for (j=0; j<3; j++)
    {
        fprintf(stderr,"%.6e ",com[j]);
    }
    fprintf(stderr,"\n");
    fprintf(stderr,"VCOM: ");
    for (j=0; j<3; j++)
    {
        fprintf(stderr,"%.6e ",vcom[j]);
    }
    fprintf(stderr," M=%.6e\n",mass);

    // Move all particles to the COM reference frame
    for (i=0; i<N; i++)
    {
        // R_new = R_old - R_com
        for (j=0; j<3; j++)
        {
            in->gp[i].pos[j] -= com[j];
            in->gp[i].vel[j] -= vcom[j];
        }
    }

    Ltot = 0.0;
    Lx = 0.0;
    Ly = 0.0;
    Lz = 0.0;

    for (j=0; j<3; j++)
    {
        L[j] = 0.0;
    }

    // Write the header
    printf("%i\n", N); 

    // Calculate the angular momentum
    for (i=0; i<N; i++)
    {
        // L = m *(r x v)
        Lx = in->gp[i].mass*(in->gp[i].pos[1]*in->gp[i].vel[2]-in->gp[i].pos[2]*in->gp[i].vel[1]);
        Ly = in->gp[i].mass*(in->gp[i].pos[2]*in->gp[i].vel[0]-in->gp[i].pos[0]*in->gp[i].vel[2]);
        Lz = in->gp[i].mass*(in->gp[i].pos[0]*in->gp[i].vel[1]-in->gp[i].pos[1]*in->gp[i].vel[0]);

        printf("%15.7E\n", Lz);
#if 0
        printf("%i", i);
        for (j=0; j<3; j++)
        {
            printf("%15.7E", L[j]);
        }

        printf("\n");
#endif
        L[0] += Lx;
        L[1] += Ly;
        L[2] += Lz;
    }

    for (j=0; j<3; j++)
    {
        Ltot += L[j]*L[j];
        fprintf(stderr, "L[%i]= %15.7E ", j, L[j]);
    }

    Ltot = sqrt(Ltot);
    fprintf(stderr, "Ltot= %15.7E\n", Ltot);

    free(com);
    free(vcom);
    TipsyFinish(in);

    return 0;
}



