/*
** This program reads a Tipsy binary file and marks the particles material and origin (assuming that all particles belonging to one layer of a body are stored one after the other.
*/
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <assert.h>
#include "tipsy.h"

void main(int argc, char **argv) {
		// Tipsy library
		TCTX in;
		int type, N;
		double dSoft;
		struct base_particle *p;
		struct gas_particle *g;
		double time;
		int nMat, iMat, iMatLast, nTarget, i;

		if (argc != 2) {
			fprintf(stderr,"Usage: tipsy_mark_mat_origin <N_target> <tipsy.std\n");
			exit(1);
		}
	
		// Read command line parameters
		nTarget = atoi(argv[1]);

		// Initialize tipsy library
		TipsyInitialize(&in,0,"stdin");
		
		N = iTipsyNumParticles(in);
		time = dTipsyTime(in);

		assert(0 <= nTarget <= N);
		
		// Print header
		printf("%i\n",N);
		
		nMat = -1;
		// No material should have this number
		iMatLast = -1;

		// Read particles from the input file
        for (i = 0; i < N; i++)
        {
			p = pTipsyRead(in,&type,&dSoft);
			assert(type == TIPSY_TYPE_GAS);
			g = (struct gas_particle *)p;
			
			iMat = (int) g->metals;
			assert(0<=iMat);

			if (iMat != iMatLast)
			{
				// Found a new material
				iMatLast = iMat;
				nMat ++;
			} else if (i == nTarget-1) {
				// All particles from the first body are read
				nMat++;
			}

			printf("%i\n",nMat);
		}
		
		TipsyFinish(in);
}
